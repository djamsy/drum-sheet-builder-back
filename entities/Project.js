// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const fs = require('fs')
const ejs = require('ejs')
const pdf = require('html-pdf')
const Lame = require('node-lame').Lame

// Functions.
const fns = require('../utils/functions')

// Model.
const Project = require('../model').Project

// Datasets.
const icons = require('../datasets/icons')
const constants = require('../datasets/constants')
const instruments = require('../datasets/instruments').asArray

// Utils.
const Synthesizer = require('../utils/synthesizer')

// =============================================================================
// Main.
// =============================================================================

// Class definition.
class ProjectEntity {
  // Get all projects from database.
  static getAll () {
    return new Promise((resolve, reject) => {
      Project.find({}, (error, projects) => {
        if (error) return reject(error)
        resolve(projects)
      })
    })
  }

  // Get single project by ID.
  static getById (id) {
    return new Promise((resolve, reject) => {
      Project.findById(id, (error, project) => {
        if (error) return reject(error)
        resolve(project)
      })
    })
  }

  // Get single project by user ID.
  static getByUserId (userId) {
    return new Promise((resolve, reject) => {
      Project.find({ userId: userId }, (error, projects) => {
        if (error) return reject(error)
        resolve(projects)
      })
    })
  }

  // Create a project.
  static create (projectObject) {
    return new Promise((resolve, reject) => {
      // Create model.
      let projectModel = new Project(projectObject)
      // Execute query.
      projectModel.save((error, project) => {
        if (error) return reject(error)
        resolve(project)
      })
    })
  }

  // Update a project.
  static update (projectModel) {
    return new Promise((resolve, reject) => {
      // Execute query.
      projectModel.save((error, project) => {
        if (error) return reject(error)
        resolve(project)
      })
    })
  }

  // Remove a project.
  static delete (projectId) {
    return new Promise((resolve, reject) => {
      // Delete project.
      Project.deleteOne({ _id: projectId }, error => {
        if (error) return reject(error)
        resolve()
      })
    })
  }

  // Get PDF options.
  static getPdfOptions () {
    return {
      format: 'A3',
      border: {
        top: '30px',
        right: '30px',
        bottom: '30px',
        left: '30px'
      },
      footer: {
        height: '16px',
        contents: {
          default: '<span style="color: #777">{{page}}</span>/<span style="color: #111">{{pages}}</span>'
        }
      }
    }
  }

  // Generate project audio.
  static generateAudio (project) {
    return new Promise((resolve, reject) => {
      // Load the synthesizer object.
      const DSBSynthesizer = new Synthesizer(constants.defaultAudioLib)
      // Load the audios.
      DSBSynthesizer.synthesize(project)
        .then(resolve)
        .catch(reject)
    })
  }

  // Compress audio from a project.
  static compressAudio (inputLocation, project) {
    return new Promise((resolve, reject) => {
      // Init the output location.
      let outputLocation = fns.getMp3FileLocation(project._id)
      // Init the encoder object.
      const mp3Encoder = new Lame({
        output: outputLocation,
        bitrate: constants.DEFAULT_MP3_BITRATE
      }).setFile(inputLocation)
      // Encode the file.
      mp3Encoder
        .encode()
        .then(() => resolve(outputLocation))
        .catch(reject)
    })
  }

  // Render project sheets.
  static renderSheets (project, email, type) {
    return new Promise((resolve, reject) => {
      // Get the instrument icons.
      icons.load()
        .then(icons => {
          // Read the template file.
          const template = fs.readFileSync(fns.getTemplateFileLocation('sheets', type), 'utf8')
          // Init the data object to pass to the renderer.
          let data = _.extend(project, {
            _: _,
            style: fns.getTemplateStyleLocation('sheets', type),
            author: email,
            icons: icons,
            lineTypes: constants.lineTypes,
            instruments: instruments,
            baseUrl: config.SERVER_URL,
            instrumentSize: fns.calculateInstrumentSize(constants.timeSignatures[project.timeSignature].beats, constants.timeSignatures[project.timeSignature].bars),
            getCellBorder: fns.getCellBorder,
            orderInstruments: fns.orderInstruments
          })
          // Wrap inside try/catch block to handle ejs rendering errors.
          try {
            const result = ejs.render(template, data)
            // Init the PDF file name.
            let pdfFile = fns.getPdfFileLocation(project._id)
            // Render PDF.
            pdf.create(result, this.getPdfOptions()).toFile(pdfFile, error => {
              if (error) return reject(error)
              // Return the file location.
              return resolve(pdfFile)
            })
          } catch (error) {
            reject(error)
          }
        })
        .catch(reject)
    })
  }
}

// Export.
module.exports = ProjectEntity
