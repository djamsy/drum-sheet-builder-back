// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const fs = require('fs')
const ejs = require('ejs')
const path = require('path')
const nodemailer = require('nodemailer')

// Functions.
const fns = require('../utils/functions')

// Utils.
const sanitizer = require('../utils/sanitizer')

// =============================================================================
// Main.
// =============================================================================

// Class definition.
class MailerEntity {
  constructor () {
    // Init.
    this.init()
  }

  // Init function.
  init () {
    // Init the transport object.
    this.transport = {
      service: config.EMAIL_SERVICE,
      auth: {
        user: config.EMAIL_USER,
        pass: config.EMAIL_PASSWORD
      }
    }
    // Init the transporter.
    this.transporter = nodemailer.createTransport(this.transport)
    // Init the types.
    this.types = {
      RECOVERY: { template: 'recovery', subject: 'Reset your password' }
    }
  }

  // Get the mail tyeps.
  static types () {
    // Create new object.
    let mailer = new MailerEntity()
    // Return the object types.
    return mailer.types
  }

  static send (to, props, replacements) {
    return new Promise((resolve, reject) => {
      // If e-mail is avoided, exit.
      if (process.env.NO_EMAIL_SEND) return resolve()
      // Get the template location.
      const templateLocation = fns.getTemplateFileLocation('email', props.template)
      // Check existence of the template.
      fs.access(templateLocation, fs.constants.F_OK, error => {
        if (error) return reject(error)
        // Read the template file.
        const template = fs.readFileSync(templateLocation, 'utf8')
        // Init the email replacements.
        let allEmailReplacements = {
          ...replacements,
          title: props.subject,
          version: config.project.version,
          baseUrl: fns.getFrontEndUrl(),
          filePath: path.dirname(templateLocation),
          assetsUrl: fns.getAssetsUrl()
        }
        // Wrap inside try/catch block to handle ejs rendering errors.
        try {
          log.debug(`Rendering template ${templateLocation} with vars: ${JSON.stringify(replacements)}.`)
          const html = ejs.render(template, allEmailReplacements)
          // Create entity.
          let mailer = new MailerEntity()
          // Init the mail options.
          let mailOptions = {
            from: config.EMAIL_USER,
            to: process.env.NODE_ENV === 'development' ? config.DEV_EMAIL : to,
            subject: `DrumSheetBuilder: ${props.subject}`,
            html: html,
            text: sanitizer.html.removeTags(html)
          }
          log.debug(`Sending mail to ${mailOptions.to} with subject "${mailOptions.subject}".`)
          // Send the e-mail.
          mailer.transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              log.error(`E-mail of type ${props.template} could not be delivered to ${mailOptions.to}: ${JSON.stringify(error)}.`)
              return reject(error)
            }
            log.info(`E-mail of type ${props.template} successfully issued for delivery to ${mailOptions.to}: ${JSON.stringify(info)}.`)
            resolve(info)
          })
        } catch (error) {
          log.error(`Error rendering EJS template ${templateLocation} with vars ${JSON.stringify(allEmailReplacements)}: ${error}.`)
          reject(error)
        }
      })
    })
  }
}

// Export.
module.exports = MailerEntity
