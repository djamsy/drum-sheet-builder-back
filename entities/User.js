// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const jwt = require('jsonwebtoken')

// Functions.
const fns = require('../utils/functions')

// Model.
const User = require('../model').User

// Entities.
const Mail = require('./Mail')

// =============================================================================
// Constants.
// =============================================================================

// Fields that will be returned in JSON responses.
const SIGNIFICANT_FIELDS = ['_id', 'email', 'role', 'termsAccepted', 'createdAt', 'updatedAt']

// Fields that will be returned in JSON responses when called by an admin user.
const ADMIN_SIGNIFICANT_FIELDS = ['_id', 'email', 'role', 'termsAccepted', 'recoveryToken', 'createdAt', 'updatedAt']

// Fields that will be encoded whithin the JWT.
const TOKEN_FIELDS = ['_id', 'email']

// =============================================================================
// Main.
// =============================================================================

// Class definition.
class UserEntity {
  // Get all users from database.
  static getAll () {
    return new Promise((resolve, reject) => {
      User.find({}, (error, users) => {
        if (error) return reject(error)
        // Return just the desired fields.
        let filteredUsers = users.map(user => this.getAdminSignificantFields(user))
        // Return.
        return resolve(filteredUsers)
      })
    })
  }

  // Get single user by e-mail.
  static getByEmail (email) {
    return new Promise((resolve, reject) => {
      User.findOne({ email: email }, (error, user) => {
        if (error) return reject(error)
        resolve(user)
      })
    })
  }

  // Get single user by ID.
  static getById (id) {
    return new Promise((resolve, reject) => {
      User.findById(id, (error, user) => {
        if (error) return reject(error)
        resolve(user)
      })
    })
  }

  // Create a user.
  static create (userObject) {
    return new Promise((resolve, reject) => {
      // Create model.
      let userModel = new User(userObject)
      // Execute query.
      userModel.save((error, user) => {
        if (error) return reject(error)
        resolve(user)
      })
    })
  }

  // Generate a token.
  static encode (user, referrer) {
    return new Promise(resolve => {
      // Get the desired user fields.
      let filteredUser = this.getTokenFields(user)
      // Create a token, saving the filtered user and the referrer.
      let token = jwt.sign({
        ...filteredUser,
        referrer: referrer || '*'
      }, config.SESSION_SECRET, {
        expiresIn: config.TOKEN_EXPIRATION /* a week */
      })
      // Return the user object, plus the token.
      resolve({
        token: token,
        expiresIn: config.TOKEN_EXPIRATION,
        ...filteredUser
      })
    })
  }

  // Decode a token.
  static decode (token) {
    return new Promise((resolve, reject) => {
      // Verify the token.
      jwt.verify(token, config.SESSION_SECRET, (error, decodedUser) => {
        if (error) return reject(error)
        resolve(decodedUser)
      })
    })
  }

  // Get fields to encode in token.
  static getTokenFields (user) {
    return _.pick(user, TOKEN_FIELDS)
  }

  // Get ony the significant fields from the user object.
  static getSignificantFields (user) {
    return _.pick(user, SIGNIFICANT_FIELDS)
  }

  // Get ony the significant fields from the user object for admin users.
  static getAdminSignificantFields (user) {
    return _.pick(user, ADMIN_SIGNIFICANT_FIELDS)
  }

  // Set a user in recovery state.
  static setRecovery (userModel) {
    return new Promise((resolve, reject) => {
      // Assign the recovery token.
      userModel.recoveryToken = fns.generateToken()
      log.debug(`Generated recovery token for user ${userModel.email}: ${userModel.recoveryToken}`)
      // Execute query.
      userModel.save((error, user) => {
        if (error) return reject(error)
        // Build the e-mail replacements.
        let replacements = {
          user: this.getSignificantFields(user),
          recoveryLink: `${fns.getFrontEndUrl()}/recovery?token=${user.recoveryToken}&email=${encodeURI(user.email)}`
        }
        // Send recovery e-mail.
        Mail.send(userModel.email, Mail.types().RECOVERY, replacements)
          .then(() => {
            resolve(user)
          })
          .catch(reject)
      })
    })
  }

  // Update a user's password.
  static resetPassword (userModel, password) {
    return new Promise((resolve, reject) => {
      // Remove the recovery token.
      userModel.recoveryToken = undefined
      userModel.password = password
      // Execute query.
      userModel.save((error, user) => {
        if (error) return reject(error)
        resolve(user)
      })
    })
  }
}

// Export.
module.exports = UserEntity
