// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const mongoose = require('mongoose')

// =============================================================================
// Aux. functions.
// =============================================================================

// Get the database URL.
const getMongoURL = (host, port, db) => `mongodb://${host}:${port}/${db}`

// =============================================================================
// Main.
// =============================================================================

// Load function.
const load = () => new Promise((resolve, reject) => {
  // Database URL.
  let DB_URL = getMongoURL(config.DB_HOST, config.DB_PORT, config.DB_NAME)
  log.debug(`Connecting to MongoDB on ${DB_URL}.`)
  // Connect to Mongo.
  mongoose.connect(DB_URL, {
    auth: {
      authdb: 'admin',
      user: config.DB_USER,
      password: config.DB_PASS
    }
  })
  // Handle connection error.
  mongoose.connection.on('error', reject)
  // Open connection.
  mongoose.connection.once('open', () => {
    // Log and resolve.
    log.info('Loaded module Mongoose.js.')
    resolve()
  })
})

// Destroy function.
const destroy = () => new Promise(resolve => {
  // Close connection.
  mongoose.connection.close()
  // Log and resolve.
  log.info('Disconnected from MongoDB.')
  resolve()
})

// Export.
module.exports = {
  order: 0,
  load: load,
  destroy: destroy
}
