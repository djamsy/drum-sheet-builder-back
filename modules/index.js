// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const path = require('path')
const glob = require('glob')
const util = require('util')

// =============================================================================
// Aux. functions.
// =============================================================================

// Promisify glob.
const find = util.promisify(glob)

// Find all .js files in same folder.
const getModules = () => {
  return new Promise((resolve, reject) => {
    // Find all .js files in same folder.
    find(path.join(__dirname, '*.js'))
      .then(files => {
        // Init the modules object.
        let modules = []
        // Require every file and append to modules object.
        files.forEach(file => {
          let fileName = path.basename(file)
          if (fileName !== 'index.js') {
            modules.push({
              name: fileName,
              ...require(file)
            })
          }
        })
        // Order modules.
        let orderedModules = _.orderBy(modules, 'order')
        resolve(orderedModules)
      })
      .catch(reject)
  })
}

// =============================================================================
// Main.
// =============================================================================

// Load function.
const load = () => new Promise((resolve, reject) => {
  // Find all .js files in same folder.
  getModules()
    .then(modules => {
      // Get the load function and append to a promise array.
      let loadFunctions = modules.map(module => module.load())
      // Execute all promises.
      Promise.all(loadFunctions)
        .then(resolve)
        .catch(reject)
    })
    .catch(reject)
})

// Destroy function.
const destroy = () => new Promise((resolve, reject) => {
  // Find all .js files in same folder.
  getModules()
    .then(modules => {
      // Get the destroy function (if exists) and append to a promise array.
      let destroyFunctions = modules.map(module => {
        if (module.destroy) return module.destroy()
      })
      // Execute all promises.
      Promise.all(destroyFunctions)
        .then(resolve)
        .catch(reject)
    })
    .catch(reject)
})

// Export.
module.exports = {
  load: load,
  destroy: destroy
}
