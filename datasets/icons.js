/**
 * This file loads all the icons from the different types of hit of a drum, by
 * reading the file names specified in property 'icons' of each hit of the
 * instruments in './instruments.js'.
 */
// =============================================================================
// Requirements.
// =============================================================================

// Vendor.
const fs = require('fs')
const async = require('async')

// Constants.
const instruments = require('./instruments').asArray

// =============================================================================
// Main class.
// =============================================================================

// Load all icons for the different instruments.
const load = () => {
  return new Promise((resolve, reject) => {
    // Init the result.
    let result = {}
    // For every instrument.
    async.each(instruments, (instrument, instrumentCb) => {
      // For every hit of this instrument.
      async.each(instrument.hits, (hit, hitCb) => {
        // For every icon of this hit.
        async.forEachOf(hit.icons, (icon, iconName, iconCb) => {
          // Read the file.
          fs.readFile(icon, { encoding: 'base64' }, (error, base64Image) => {
            if (error) return iconCb(error)
            // Init results.
            if (!result[instrument.label]) result[instrument.label] = {}
            if (!result[instrument.label][hit.label]) result[instrument.label][hit.label] = {}
            // Assign to result.
            result[instrument.label][hit.label][iconName] = `data:image/jpeg;charset=utf-8;base64,${base64Image}`
            // Callback and go to next item.
            iconCb()
          })
        }, hitCb)
      }, instrumentCb)
    }, error => {
      if (error) return reject(error)
      resolve(result)
    })
  })
}

// =============================================================================
// Export.
// =============================================================================

// Export icon mapping.
module.exports = {
  load: load
}
