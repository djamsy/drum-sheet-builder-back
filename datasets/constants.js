/**
 * This file contains some of the constants defined in drum-sheet-bulder-front.
 * They must have EXACTLY the same values.
 **/

// Vendor dependencies.
const path = require('path')

// Define the constants.
const constants = {
  // Possible values of beats and bars.
  timeSignatures: {
    '2/2': { beats: 8, bars: 4, disabledBeats: [1, 2, 3, 5, 6, 7], fillEachSteps: [4, 8], shiftBeatsSize: 4 },
    '3/4': { beats: 6, bars: 4, disabledBeats: [1, 3, 5], fillEachSteps: [2, 6], stepSize: 4, shiftBeatsSize: 2 },
    '4/4': { beats: 8, bars: 4, disabledBeats: [1, 3, 5, 7], fillEachSteps: [2, 4, 8], shiftBeatsSize: 2 },
    '5/4': { beats: 10, bars: 4, disabledBeats: [1, 3, 5, 7, 9], fillEachSteps: [2, 10], shiftBeatsSize: 2 },
    '6/8': { beats: 6, bars: 4, fillEachSteps: [1, 3, 6], shiftBeatsSize: 1 },
    '7/8': { beats: 7, bars: 4, fillEachSteps: [1, 7], shiftBeatsSize: 1 },
    '8/8': { beats: 8, bars: 4, fillEachSteps: [1, 2, 4, 8], shiftBeatsSize: 1 },
    '10/8': { beats: 10, bars: 4, fillEachSteps: [1, 5, 10], shiftBeatsSize: 1 },
    '10/16': { beats: 10, bars: 2, fillEachSteps: [1, 2, 5, 10], shiftBeatsSize: 1 },
    '12/16': { beats: 12, bars: 2, fillEachSteps: [1, 3, 6, 12], shiftBeatsSize: 1 },
    '16/16': { beats: 16, bars: 2, fillEachSteps: [1, 2, 4, 8, 16], shiftBeatsSize: 1 }
  },
  // Maximum line width, in pixels.
  maxLineWidth: 900,
  // Maximum instrument size, in pixels.
  maxInstrumentSize: 24,
  // Line background colors.
  lineBgColors: {
    darkGray: { background: '#ccc', buttons: '#d3d3d3' },
    mediumGray: { background: '#ddd', buttons: '#b3b3b3' },
    lightGray: { background: '#eee', buttons: '#d3d3d3' },
    darkBlue: { background: '#c1cde6', buttons: '#b3b3b3' },
    mediumBlue: { background: '#cdd8ef', buttons: '#b3b3b3' },
    lightBlue: { background: '#dbe7ff', buttons: '#b3b3b3' },
    darkYellow: { background: '#fffacf', buttons: '#d3d3d3' },
    mediumYellow: { background: '#fffee3', buttons: '#d3d3d3' },
    lightYellow: { background: '#fffff2', buttons: '#d3d3d3' },
    darkGreen: { background: '#bed0c5', buttons: '#b3b3b3' },
    mediumGreen: { background: '#d4ded8', buttons: '#b3b3b3' },
    lightGreen: { background: '#e1e6e3', buttons: '#d3d3d3' },
    darkRed: { background: '#e0cbc8', buttons: '#b3b3b3' },
    mediumRed: { background: '#e2d5d3', buttons: '#b3b3b3' },
    lightRed: { background: '#ece4e2', buttons: '#d3d3d3' },
    darkOrange: { background: '#e8dbd7', buttons: '#b3b3b3' },
    mediumOrange: { background: '#f1e6e2', buttons: '#b3b3b3' },
    lightOrange: { background: '#f7eeeb', buttons: '#d3d3d3' }
  },
  // Separator background colors.
  separatorBgColors: {
    darkGray: '#777',
    mediumGray: '#999',
    lightGray: '#bbb'
  },
  // Line types.
  lineTypes: {
    line: 'line',
    separator: 'separator'
  },
  // Custom token charset.
  customTokenCharset: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_',
  // Default instrument order types.
  instrumentOrderTypes: {
    drumset: 'drumset',
    canonical: 'canonical'
  },
  // Instrument icon types.
  instrumentIconTypes: {
    drawings: 'drawings',
    canonical: 'canonical',
    pictures: 'pictures'
  },
  // Audio libs.
  audioLibs: {
    lofi: 'lofi'
  },
  // Default audio lib.
  defaultAudioLib: 'lofi',
  // Location of files.
  ICONS_LOCATION: path.resolve(__dirname, '../icons/instruments'),
  AUDIO_LOCATION: path.resolve(__dirname, '../audio'),
  // Factor that depends on the hit depth (divider of the time signature) to generate audio.
  hitDepthFactor: {
    1: 4,
    2: 4,
    4: 4,
    8: 4,
    16: 8
  },
  // MP3 qualities.
  MP3_QUALITIES: {
    high: 320,
    medium: 192,
    low: 128
  },
  // Default MP3 compression bitrate.
  DEFAULT_MP3_BITRATE: 192
}

// Export.
module.exports = constants
