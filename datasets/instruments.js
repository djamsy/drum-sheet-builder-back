/**
 * This file must contain an EXACT COPY of src/shared/instruments.js from
 * project drum-sheet-builder-front, but with Node.js syntax and containing
 * the route of the icon in the 'icons' attribute, under 'hits'.
 * Icons will be taken from ../icons/instruments/.
 */
// =============================================================================
// Requirements.
// =============================================================================

// Vendor.
const path = require('path')

// Constants.
const constants = require('./constants')

// =============================================================================
// Main.
// =============================================================================

// Instrument details.
const instruments = [
  {
    label: 'BD',
    name: 'Kick',
    drumsetOrder: 0,
    canonicalOrder: 8,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'BD/drawings/BD-default.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'BD/canonical/BD-default.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'BD/pictures/BD-default.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/BD/BD-default.wav')
        }
      },
      {
        name: 'Ghost note',
        label: 'ghost',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'BD/drawings/BD-ghost.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'BD/canonical/BD-ghost.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'BD/pictures/BD-ghost.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/BD/BD-ghost.wav')
        }
      }
    ]
  },
  {
    label: 'CL',
    name: 'Left Crash',
    drumsetOrder: 3,
    canonicalOrder: 0,
    hits: [
      {
        name: 'Edge',
        label: 'edge',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'CL/drawings/CL-edge.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'CL/canonical/CL-edge.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'CL/pictures/CL-edge.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/CL/CL-edge.wav')
        }
      },
      {
        name: 'Bow',
        label: 'bow',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'CL/drawings/CL-bow.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'CL/canonical/CL-bow.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'CL/pictures/CL-bow.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/CL/CL-bow.wav')
        }
      },
      {
        name: 'Bell',
        label: 'bell',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'CL/drawings/CL-bell.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'CL/canonical/CL-bell.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'CL/pictures/CL-bell.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/CL/CL-bell.wav')
        }
      }
    ]
  },
  {
    label: 'CR',
    name: 'Right Crash',
    drumsetOrder: 6,
    canonicalOrder: 1,
    hits: [
      {
        name: 'Edge',
        label: 'edge',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'CR/drawings/CR-edge.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'CR/canonical/CR-edge.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'CR/pictures/CR-edge.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/CR/CR-edge.wav')
        }
      },
      {
        name: 'Bow',
        label: 'bow',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'CR/drawings/CR-bow.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'CR/canonical/CR-bow.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'CR/pictures/CR-bow.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/CR/CR-bow.wav')
        }
      },
      {
        name: 'Bell',
        label: 'bell',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'CR/drawings/CR-bell.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'CR/canonical/CR-bell.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'CR/pictures/CR-bell.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/CR/CR-bell.wav')
        }
      }
    ]
  },
  {
    label: 'RI',
    name: 'Ride',
    drumsetOrder: 7,
    canonicalOrder: 2,
    hits: [
      {
        name: 'Edge',
        label: 'edge',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'RI/drawings/RI-edge.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'RI/canonical/RI-edge.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'RI/pictures/RI-edge.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/RI/RI-edge.wav')
        }
      },
      {
        name: 'Bow',
        label: 'bow',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'RI/drawings/RI-bow.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'RI/canonical/RI-bow.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'RI/pictures/RI-bow.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/RI/RI-bow.wav')
        }
      },
      {
        name: 'Bell',
        label: 'bell',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'RI/drawings/RI-bell.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'RI/canonical/RI-bell.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'RI/pictures/RI-bell.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/RI/RI-bell.wav')
        }
      }
    ]
  },
  {
    label: 'HH',
    name: 'Hi-Hat',
    drumsetOrder: 2,
    canonicalOrder: 3,
    hits: [
      {
        name: 'Bow, closed',
        label: 'closed-bow',
        cutPrevious: true,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'HH/drawings/HH-closed-bow.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'HH/canonical/HH-closed-bow.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'HH/pictures/HH-closed-bow.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/HH/HH-closed-bow.wav')
        }
      },
      {
        name: 'Bow, open',
        label: 'open-bow',
        cutPrevious: true,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'HH/drawings/HH-open-bow.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'HH/canonical/HH-open-bow.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'HH/pictures/HH-open-bow.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/HH/HH-open-bow.wav')
        }
      },
      {
        name: 'Pedal, closed',
        label: 'closed-pedal',
        cutPrevious: true,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'HH/drawings/HH-closed-pedal.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'HH/canonical/HH-closed-pedal.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'HH/pictures/HH-closed-pedal.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/HH/HH-closed-pedal.wav')
        }
      },
      {
        name: 'Pedal, open',
        label: 'open-pedal',
        cutPrevious: true,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'HH/drawings/HH-open-pedal.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'HH/canonical/HH-open-pedal.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'HH/pictures/HH-open-pedal.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/HH/HH-open-pedal.wav')
        }
      },
      {
        name: 'Bell, closed',
        label: 'closed-bell',
        cutPrevious: true,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'HH/drawings/HH-closed-bell.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'HH/canonical/HH-closed-bell.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'HH/pictures/HH-closed-bell.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/HH/HH-closed-bell.wav')
        }
      },
      {
        name: 'Bell, open',
        label: 'open-bell',
        cutPrevious: true,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'HH/drawings/HH-open-bell.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'HH/canonical/HH-open-bell.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'HH/pictures/HH-open-bell.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/HH/HH-open-bell.wav')
        }
      }
    ]
  },
  {
    label: 'SN',
    name: 'Snare',
    drumsetOrder: 1,
    canonicalOrder: 4,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'SN/drawings/SN-default.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'SN/canonical/SN-default.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'SN/pictures/SN-default.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/SN/SN-default.wav')
        }
      },
      {
        name: 'Ghost note',
        label: 'ghost',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'SN/drawings/SN-ghost.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'SN/canonical/SN-ghost.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'SN/pictures/SN-ghost.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/SN/SN-ghost.wav')
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'SN/drawings/SN-rim-shot.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'SN/canonical/SN-rim-shot.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'SN/pictures/SN-rim-shot.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/SN/SN-rim-shot.wav')
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'SN/drawings/SN-flam.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'SN/canonical/SN-flam.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'SN/pictures/SN-flam.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/SN/SN-flam.wav')
        }
      },
      {
        name: 'Drag',
        label: 'drag',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'SN/drawings/SN-drag.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'SN/canonical/SN-drag.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'SN/pictures/SN-drag.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/SN/SN-ghost.wav')
        },
        drag: true
      },
      {
        name: 'Side stick',
        label: 'side-stick',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'SN/drawings/SN-side-stick.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'SN/canonical/SN-side-stick.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'SN/pictures/SN-side-stick.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/SN/SN-side-stick.wav')
        }
      }
    ]
  },
  {
    label: 'TL',
    name: 'Left Tom',
    drumsetOrder: 4,
    canonicalOrder: 5,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TL/drawings/TL-default.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TL/canonical/TL-default.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TL/pictures/TL-default.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TL/TL-default.wav')
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TL/drawings/TL-rim-shot.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TL/canonical/TL-rim-shot.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TL/pictures/TL-rim-shot.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TL/TL-rim-shot.wav')
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TL/drawings/TL-flam.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TL/canonical/TL-flam.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TL/pictures/TL-flam.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TL/TL-flam.wav')
        }
      },
      {
        name: 'Head',
        label: 'head',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TL/drawings/TL-head.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TL/canonical/TL-head.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TL/pictures/TL-head.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TL/TL-head.wav')
        }
      }
    ]
  },
  {
    label: 'TR',
    name: 'Right Tom',
    drumsetOrder: 5,
    canonicalOrder: 6,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TR/drawings/TR-default.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TR/canonical/TR-default.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TR/pictures/TR-default.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TR/TR-default.wav')
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TR/drawings/TR-rim-shot.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TR/canonical/TR-rim-shot.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TR/pictures/TR-rim-shot.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TR/TR-rim-shot.wav')
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TR/drawings/TR-flam.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TR/canonical/TR-flam.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TR/pictures/TR-flam.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TR/TR-flam.wav')
        }
      },
      {
        name: 'Head',
        label: 'head',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TR/drawings/TR-head.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TR/canonical/TR-head.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TR/pictures/TR-head.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TR/TR-head.wav')
        }
      }
    ]
  },
  {
    label: 'TB',
    name: 'Base Tom',
    drumsetOrder: 8,
    canonicalOrder: 7,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TB/drawings/TB-default.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TB/canonical/TB-default.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TB/pictures/TB-default.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TB/TB-default.wav')
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TB/drawings/TB-rim-shot.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TB/canonical/TB-rim-shot.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TB/pictures/TB-rim-shot.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TB/TB-rim-shot.wav')
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TB/drawings/TB-flam.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TB/canonical/TB-flam.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TB/pictures/TB-flam.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TB/TB-flam.wav')
        }
      },
      {
        name: 'Head',
        label: 'head',
        cutPrevious: false,
        icons: {
          drawings: path.join(constants.ICONS_LOCATION, 'TB/drawings/TB-head.png'),
          canonical: path.join(constants.ICONS_LOCATION, 'TB/canonical/TB-head.png'),
          pictures: path.join(constants.ICONS_LOCATION, 'TB/pictures/TB-head.png')
        },
        audio: {
          lofi: path.join(constants.AUDIO_LOCATION, 'lofi/TB/TB-head.wav')
        }
      }
    ]
  }
]

// Get as object.
const getInstrumentsAsObject = () => {
  // Init the result.
  let r = {}
  // Loop through every instrument.
  for (let i = 0; i < instruments.length; i++) {
    let instrument = instruments[i]
    // Init the instrument attribute.
    r[instrument.label] = []
    // Loop through every hit.
    for (let j = 0; j < instrument.hits.length; j++) {
      let hit = instrument.hits[j]
      // Push to result.
      r[instrument.label].push(hit.label)
    }
  }
  return r
}

// Export.
module.exports = {
  asArray: instruments,
  asObject: getInstrumentsAsObject()
}
