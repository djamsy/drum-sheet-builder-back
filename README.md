# drum-sheet-builder-back

An API REST for [https://drumsheetbuilder.com](https://drumsheetbuilder.com).

## Dependencies

### Node.js + NPM

NPM + Node.js version 11+. This is important, for `async/await` support.

### MongoDB

Standalone MongoDB instance, secured with [username and password](https://docs.mongodb.com/manual/tutorial/enable-authentication/).

First, initialize your instance in standalone mode, replacing `${DB_PORT}` and `${DB_PATH}` with your own. You must create directory `${DB_PATH}` first.

```
mongod --port ${DB_PORT} --dbpath ${DB_PATH}
```

To secure your MongoDB, connect to the instance from the command line, in another terminal window:

```
mongo --port ${DB_PORT}
```

Then use the `admin` database, and when connected, perform the following query to create the database **administrator**, replacing `${DB_ADMIN_USER}` and `${DB_ADMIN_PASS}` with your own:

```
use admin
db.createUser({ user:"${DB_ADMIN_USER}", pwd:"${DB_ADMIN_PASS}", roles:[{ role:"userAdminAnyDatabase", db:"admin" }, "readWriteAnyDatabase"] })
```

Then, exit the shell and restart instance in authentication mode, replacing `${DB_PORT}` and `${DB_PATH}` with your own. You must provide the credentials for the created admin user.

```
mongod --auth --port ${DB_PORT} --dbpath ${DB_PATH}
```

Connect to the instance with authentication:

```
mongo --port ${DB_PORT}
```

and then authenticate with the following command, replacing `${DB_USER}` and `${DB_PASS}` with your own:

```
use admin
db.auth("{DB_USER}", "${DB_PASS}")
```

Then, create the database and user for this project and grant privileges to it, replacing `${DB_USER}`, `${DB_PASS}` and `${DB_NAME}` with your own:

```
use ${DB_NAME}
db.createUser({ user: "${DB_USER}", pwd: "${DB_PASS}", roles: [ { role: "readWrite", db: "${DB_NAME}" }] })
```

Finally, restart the instance, replacing `${DB_PORT}` and `${DB_PATH}` with your own:

```
mongod --auth --port ${DB_PORT} --dbpath ${DB_PATH}
```
