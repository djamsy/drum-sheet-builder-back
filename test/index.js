// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const winston = require('winston')

// Modules.
const modules = require('../modules')

// Utils.
const fns = require('../utils/functions')

// Main Test class.
const AuthTests = require('./classes/AuthTests')
const UserTests = require('./classes/UserTests')
const ProjectTests = require('./classes/ProjectTests')

// =============================================================================
// Initialization.
// =============================================================================

// Init the root directory.
const ROOT = path.resolve(__dirname, '..')

// Check for existence of .env file.
try {
  fs.accessSync(path.join(ROOT, '.env'), fs.constants.F_OK)
  require('dotenv').config()
} catch (err) {
  console.log(`${chalk.red('error')} File ${chalk.bold('.env')} not found in ${chalk.underline(ROOT)}.`)
  process.exit(0)
}

// Get config from read file.
const config = fns.loadConfig(process.env.NODE_ENV)

// Configure winston log.
const log = winston.createLogger({
  level: config.LOG_LEVEL,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      )
    })
  ]
})

// Export global variables.
global.log = log
global.config = config

// Export custom environment variables.
process.env.NO_EMAIL_SEND = true

// =============================================================================
// Main.
// =============================================================================

// Load modules before.
before(done => {
  modules.load()
    .then(() => done())
    .catch(done)
})

// Execute tests.
AuthTests.exec()
UserTests.exec()
ProjectTests.exec()

// Exit the process manually, if it didn't exit by itself.
after(() => {
  setTimeout(() => {
    process.exit(0)
  }, 1000)
})
