// Import common fixtures.
const { dummyInt, dummyString, emptyString } = require('./common')

// Export fixtures.
module.exports = {
  authToken: {
    wrongType: dummyInt,
    malformed: '34r7634763476r346r'
  },
  email: {
    wrongType: dummyInt,
    malformed: dummyString,
    nonExistent: `test_${Date.now()}@drumsheetbuilder.com`
  },
  password: {
    wrongType: dummyInt,
    malformed: emptyString,
    nonExistent: dummyString,
    good: 'password'
  },
  termsAccepted: {
    wrongType: dummyString,
    malformed: false,
    good: true
  },
  role: {
    wrongType: dummyInt,
    malformed: emptyString,
    good: 'user'
  },
  recoveryToken: {
    wrongType: dummyInt,
    malformed: dummyString,
    nonExistent: 'q1w2e3r4t5y6u7i8o9p0Q1W2E3R4T5Y6'
  }
}
