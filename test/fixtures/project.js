// Import common fixtures.
const { dummyInt, dummyString, emptyString, negativeInt } = require('./common')

// Export fixtures.
module.exports = {
  _id: {
    nonExistent: '50a0907f8d77750c600f120f'
  },
  title: {
    wrongType: dummyInt,
    malformed: emptyString,
    good: `Test project ${Date.now()}`
  },
  timeSignature: {
    wrongType: dummyInt,
    malformed: dummyString,
    good: '8/8'
  },
  bpm: {
    wrongType: dummyString,
    malformed: negativeInt,
    good: 120
  },
  lines: {
    wrongType: dummyInt,
    malformed: [
      {
        type: 'line',
        content: [
          { BD: [1, 0, 0, 0, 0, 0, 0, 999] },
          { SN: [0, 0, 0, 0, 1, 0, 0, 0] },
          { HH: [1, 0, 1, 0, 1, 0, 0, 1] },
          { CL: [0, 0, 0, 0, 0, 0, 0, 0] },
          { CR: [0, 0, 0, 0, 0, 0, 0, 0] },
          { RI: [0, 0, 0, 0, 0, 0, 0, 0] },
          { TL: [0, 0, 0, 0, 0, 0, 0, 0] },
          { TR: [0, 0, 0, 0, 0, 0, 0, 0] },
          { TB: [0, 0, 0, 0, 0, 0, 0, 0] }
        ],
        bgColor: '#d4ded8',
        collapsed: false
      }
    ],
    good: [
      {
        type: 'line',
        content: [
          { BD: [1, 0, 0, 0, 0, 0, 0, 0] },
          { SN: [0, 0, 0, 0, 1, 0, 0, 0] },
          { HH: [1, 0, 1, 0, 1, 0, 0, 1] },
          { CL: [0, 0, 0, 0, 0, 0, 0, 0] },
          { CR: [0, 0, 0, 0, 0, 0, 0, 0] },
          { RI: [0, 0, 0, 0, 0, 0, 0, 0] },
          { TL: [0, 0, 0, 0, 0, 0, 0, 0] },
          { TR: [0, 0, 0, 0, 0, 0, 0, 0] },
          { TB: [0, 0, 0, 0, 0, 0, 0, 0] }
        ],
        bgColor: '#d4ded8',
        collapsed: false
      }
    ]
  },
  instrumentOrder: {
    wrongType: dummyInt,
    malformed: dummyString,
    good: 'canonical'
  },
  instrumentIcons: {
    wrongType: dummyInt,
    malformed: dummyString,
    good: 'canonical'
  },
  showGrid: {
    wrongType: dummyString,
    good: true
  }
}
