// Export common fixtures.
module.exports = {
  dummyString: 'paco',
  emptyString: '    ',
  dummyInt: 10,
  negativeInt: -1
}
