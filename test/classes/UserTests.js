// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const { expect } = require('chai')

// Fixtures.
const fixtures = require('../fixtures/user')

// Entitites.
const User = require('../../entities/User')

// Test core class.
const TestCore = require('./TestCore')

// Utils.
const fns = require('../../utils/functions')

// =============================================================================
// Main.
// =============================================================================

// Main class.
class UserTests {
  static exec () {
    // Init the recovery token.
    let recoveryToken = null

    describe('POST /user', () => {
      // Init the default fields object.
      let body = {
        email: fixtures.email.nonExistent,
        password: fixtures.password.good,
        termsAccepted: fixtures.termsAccepted.good,
        role: fixtures.role.good
      }
      // Init the auth fields array.
      let bodyFieldNames = _.keys(body)
      // Init the required fields array.
      let requiredFields = ['email', 'password', 'termsAccepted']
      // Check empty body.
      it(`should return status 400 when empty body`, done => {
        TestCore.axios().post('/user', {})
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(400, data, done))
      })
      // Loop through every field.
      for (let i in bodyFieldNames) {
        if (bodyFieldNames.hasOwnProperty(i)) {
          // Get the field name.
          let field = bodyFieldNames[i]
          // Check required field.
          if (requiredFields.indexOf(field) !== -1) {
            it(`should return status 400 when no "${field}"`, done => {
              TestCore.axios().post('/user', _.omit(body, [field]))
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check wrong type.
          if (fixtures[field].wrongType) {
            it(`should return status 400 when wrong type for field "${field}"`, done => {
              TestCore.axios().post('/user', { ...body, [field]: fixtures[field].wrongType })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check malformed value.
          if (fixtures[field].malformed) {
            it(`should return status 400 when malformed "${field}"`, done => {
              TestCore.axios().post('/user', { ...body, [field]: fixtures[field].malformed })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
        }
      }
      // Check failure when existing e-mail.
      it(`should return status 403 when existing e-mail`, done => {
        TestCore.axios().post('/user', { ...body, email: config.TEST_USER_EMAIL })
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      // Good fields should return a 200 OK.
      it(`should return status 200 when good user fields`, done => {
        TestCore.axios().post('/user', body)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('email')
              expect(response.data).to.have.property('token')
              expect(response.data).to.have.property('expiresIn')
              expect(response.data.expiresIn).to.be.a('number')
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('POST /recovery', () => {
      it(`should return status 400 when empty body`, done => {
        TestCore.axios().post('/recovery', {})
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(400, data, done))
      })
      it(`should return status 400 when wrong type for field "email"`, done => {
        TestCore.axios().post('/recovery', { email: fixtures.email.wrongType })
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(400, data, done))
      })
      it(`should return status 400 when malformed "email"`, done => {
        TestCore.axios().post('/recovery', { email: fixtures.email.malformed })
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(400, data, done))
      })
      it(`should return status 403 when non-existing e-mail`, done => {
        TestCore.axios().post('/recovery', { email: 'paco@paco.es' })
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 200 when existing e-mail`, done => {
        TestCore.axios().post('/recovery', { email: fixtures.email.nonExistent }) /* the recently created e-mail */
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('_id')
              expect(response.data).to.have.property('email')
              expect(response.data).to.have.property('role')
              expect(response.data).to.have.property('termsAccepted')
              expect(response.data).to.have.property('createdAt')
              expect(response.data).to.have.property('updatedAt')
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('POST /recover', () => {
      // Get the recovery token before.
      before(done => {
        User.getByEmail(fixtures.email.nonExistent)
          .then(user => {
            recoveryToken = user.recoveryToken
            done()
          })
          .catch(error => {
            done(new Error(`Error getting user by email ${fixtures.email.nonExistent}: ${fns.stringifyError(error)}.`))
          })
      })
      // Init the default fields object.
      let body = {
        email: fixtures.email.nonExistent /* the recently created e-mail */,
        password: fixtures.password.good
      }
      // Init the auth fields array.
      let bodyFieldNames = _.keys(body)
      // Init the required fields array.
      let requiredFields = ['email', 'password', 'recoveryToken']
      // Check empty body.
      it(`should return status 400 when empty body`, done => {
        TestCore.axios().post('/recover', {})
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(400, data, done))
      })
      // Loop through every field.
      for (let i in bodyFieldNames) {
        if (bodyFieldNames.hasOwnProperty(i)) {
          // Get the field name.
          let field = bodyFieldNames[i]
          // Check required field.
          if (requiredFields.indexOf(field) !== -1) {
            it(`should return status 400 when no "${field}"`, done => {
              TestCore.axios().post('/recover', _.omit(body, [field]))
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check wrong type.
          if (fixtures[field].wrongType) {
            it(`should return status 400 when wrong type for field "${field}"`, done => {
              TestCore.axios().post('/recover', { ...body, [field]: fixtures[field].wrongType })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check malformed value.
          if (fixtures[field].malformed) {
            it(`should return status 400 when malformed "${field}"`, done => {
              TestCore.axios().post('/recover', { ...body, [field]: fixtures[field].malformed })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
        }
      }
      // Check failure when no recovery token.
      it(`should return status 401 when no recovery token`, done => {
        TestCore.axios().post('/recover', body)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      // Check failure when wrong recovery token.
      it(`should return status 401 when wrong recovery token`, done => {
        TestCore.axios().post('/recover', { ...body, recoveryToken: fixtures.recoveryToken.nonExistent })
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      // Check failure when existing e-mail.
      it(`should return status 403 when non-existent e-mail`, done => {
        TestCore.axios().post('/recover', { ...body, recoveryToken, email: 'paco@paco.es' })
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      // Good fields should return a 200 OK.
      it(`should return status 200 when good user fields`, done => {
        TestCore.axios().post('/recover', { ...body, recoveryToken })
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('_id')
              expect(response.data).to.have.property('email')
              expect(response.data).to.have.property('role')
              expect(response.data).to.have.property('termsAccepted')
              expect(response.data).to.have.property('createdAt')
              expect(response.data).to.have.property('updatedAt')
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })
  }
}

// Export.
module.exports = UserTests
