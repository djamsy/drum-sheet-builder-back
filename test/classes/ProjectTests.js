// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const { expect } = require('chai')

// Fixtures.
const fixtures = require('../fixtures/project')

// Entitites.
const Project = require('../../entities/Project')

// Test core class.
const TestCore = require('./TestCore')

// =============================================================================
// Main.
// =============================================================================

// Init some variables.
let token = null
let otherProjectId = null
let createdProjectId = null

// Init the default fields object.
let body = {
  title: fixtures.title.good,
  timeSignature: fixtures.timeSignature.good,
  bpm: fixtures.bpm.good,
  lines: fixtures.lines.good,
  instrumentOrder: fixtures.instrumentOrder.good,
  instrumentIcons: fixtures.instrumentIcons.good,
  showGrid: fixtures.showGrid.good
}

// Init the auth fields array.
let bodyFieldNames = _.keys(body)

// Init the required fields array.
let requiredFields = ['title', 'timeSignature', 'bpm', 'lines', 'instrumentOrder', 'instrumentIcons']

// Main class.
class ProjectTests {
  static exec () {
    // Find the ID of an existing project.
    before(done => {
      Project.getAll()
        .then(projects => {
          otherProjectId = _.head(projects)._id
          done()
        })
        .catch(done)
    })

    // Login.
    before(done => {
      TestCore.axios().post('/login', { email: config.TEST_USER_EMAIL, password: config.TEST_USER_PASS })
        .then(response => {
          try {
            expect(response.status).to.equal(200)
            expect(response.data).to.have.property('email')
            expect(response.data).to.have.property('token')
            expect(response.data).to.have.property('expiresIn')
            expect(response.data.expiresIn).to.be.a('number')
            // Save token once all expectations have been fulfilled.
            token = response.data.token
            done()
          } catch (error) {
            done(error)
          }
        })
        .catch(done)
    })

    describe('POST /project', () => {
      // Check non-authenticated.
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).post('/project', body)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      // Check empty body.
      it(`should return status 400 when empty body`, done => {
        TestCore.axios(token).post('/project', {})
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(400, data, done))
      })
      // Loop through every field.
      for (let i in bodyFieldNames) {
        if (bodyFieldNames.hasOwnProperty(i)) {
          // Get the field name.
          let field = bodyFieldNames[i]
          // Check required field.
          if (requiredFields.indexOf(field) !== -1) {
            it(`should return status 400 when no "${field}"`, done => {
              TestCore.axios(token).post('/project', _.omit(body, [field]))
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check wrong type.
          if (fixtures[field].wrongType) {
            it(`should return status 400 when wrong type for field "${field}"`, done => {
              TestCore.axios(token).post('/project', { ...body, [field]: fixtures[field].wrongType })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check malformed value.
          if (fixtures[field].malformed) {
            it(`should return status 400 when malformed "${field}"`, done => {
              TestCore.axios(token).post('/project', { ...body, [field]: fixtures[field].malformed })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
        }
      }
      // Good fields should return a 200 OK.
      it(`should return status 200 when good body fields`, done => {
        TestCore.axios(token).post('/project', body)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              for (let i = 0; i < bodyFieldNames.length; i++) {
                expect(response.data).to.have.property(bodyFieldNames[i])
              }
              // Save the ID of the created project.
              expect(response.data).to.have.property('_id')
              createdProjectId = response.data._id
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('GET /project/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).get(`/project/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios(token).get(`/project/${otherProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios(token).get(`/project/${fixtures._id.nonExistent}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      it(`should return status 200 when user is the owner`, done => {
        TestCore.axios(token).get(`/project/${createdProjectId}`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              for (let i = 0; i < bodyFieldNames.length; i++) {
                expect(response.data).to.have.property(bodyFieldNames[i])
              }
              // Save the ID of the created project.
              expect(response.data).to.have.property('_id')
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('PATCH /project/:id', () => {
      // Check non-authenticated.
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios(token).patch(`/project/${otherProjectId}`, body)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      // Check non-authenticated.
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).patch(`/project/${createdProjectId}`, body)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      // Check empty body.
      it(`should return status 400 when empty body`, done => {
        TestCore.axios(token).patch(`/project/${createdProjectId}`, {})
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(400, data, done))
      })
      // Loop through every field.
      for (let i in bodyFieldNames) {
        if (bodyFieldNames.hasOwnProperty(i)) {
          // Get the field name.
          let field = bodyFieldNames[i]
          // Check required field.
          if (requiredFields.indexOf(field) !== -1) {
            it(`should return status 400 when no "${field}"`, done => {
              TestCore.axios(token).patch(`/project/${createdProjectId}`, _.omit(body, [field]))
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check wrong type.
          if (fixtures[field].wrongType) {
            it(`should return status 400 when wrong type for field "${field}"`, done => {
              TestCore.axios(token).patch(`/project/${createdProjectId}`, { ...body, [field]: fixtures[field].wrongType })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
          // Check malformed value.
          if (fixtures[field].malformed) {
            it(`should return status 400 when malformed "${field}"`, done => {
              TestCore.axios(token).patch(`/project/${createdProjectId}`, { ...body, [field]: fixtures[field].malformed })
                .then(response => TestCore.rejectHttpStatus(response.status, done))
                .catch(data => TestCore.assertResponseStatus(400, data, done))
            })
          }
        }
      }
      it(`should return status 200 when good body fields`, done => {
        TestCore.axios(token).patch(`/project/${createdProjectId}`, body)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              for (let i = 0; i < bodyFieldNames.length; i++) {
                expect(response.data).to.have.property(bodyFieldNames[i])
              }
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('POST /pdf/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).post(`/pdf/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios(token).post(`/pdf/${otherProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios(token).post(`/pdf/${fixtures._id.nonExistent}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      it(`should return status 200 when user is the owner`, done => {
        TestCore.axios(token).post(`/pdf/${createdProjectId}`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('url')
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('GET /pdf/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).get(`/pdf/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios().get(`/pdf/${otherProjectId}?token=${token}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios().get(`/pdf/${fixtures._id.nonExistent}?token=${token}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      it(`should return status 200 when user is the owner`, done => {
        TestCore.axios().get(`/pdf/${createdProjectId}?token=${token}`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('POST /wav/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).post(`/wav/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios(token).post(`/wav/${otherProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios(token).post(`/wav/${fixtures._id.nonExistent}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      it(`should return status 200 when user is the owner`, done => {
        TestCore.axios(token).post(`/wav/${createdProjectId}`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('file')
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('GET /wav/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).get(`/wav/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios().get(`/wav/${otherProjectId}?token=${token}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios().get(`/wav/${fixtures._id.nonExistent}?token=${token}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      it(`should return status 200 when user is the owner`, done => {
        TestCore.axios().get(`/wav/${createdProjectId}?token=${token}`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('POST /mp3/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).post(`/mp3/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios(token).post(`/mp3/${otherProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios(token).post(`/mp3/${fixtures._id.nonExistent}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      it(`should return status 200 when user is the owner`, done => {
        TestCore.axios(token).post(`/mp3/${createdProjectId}`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('file')
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('GET /mp3/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).get(`/mp3/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios().get(`/mp3/${otherProjectId}?token=${token}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios().get(`/mp3/${fixtures._id.nonExistent}?token=${token}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      it(`should return status 200 when user is the owner`, done => {
        TestCore.axios().get(`/mp3/${createdProjectId}?token=${token}`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('GET /projects', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).get(`/projects`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 200 and non-empty list`, done => {
        TestCore.axios(token).get(`/projects`)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.not.equal([])
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('DELETE /project/:id', () => {
      it(`should return status 401 when not authenticated`, done => {
        TestCore.axios(null).delete(`/project/${createdProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it(`should return status 403 when user is not the owner`, done => {
        TestCore.axios(token).delete(`/project/${otherProjectId}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(403, data, done))
      })
      it(`should return status 404 when project does not exist`, done => {
        TestCore.axios(token).delete(`/project/${fixtures._id.nonExistent}`)
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(404, data, done))
      })
      // Good request should return a 200 OK.
      it(`should return status 204 when user is the owner`, done => {
        TestCore.axios(token).delete(`/project/${createdProjectId}`)
          .then(response => {
            try {
              expect(response.status).to.equal(204)
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })
  }
}

// Export.
module.exports = ProjectTests
