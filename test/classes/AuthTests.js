// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const { expect } = require('chai')

// Fixtures.
const fixtures = require('../fixtures/user')

// Test core class.
const TestCore = require('./TestCore')

// =============================================================================
// Main.
// =============================================================================

// Main class.
class AuthTests {
  static exec () {
    // Auth token.
    let token = null

    describe('GET /user', () => {
      it('should return status 401 when no token', done => {
        TestCore.axios(token).get('/user')
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(401, data, done))
      })
      it('should return status 500 with token of wrong type', done => {
        TestCore.axios(fixtures.authToken.wrongType).get('/user')
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(500, data, done))
      })
      it('should return status 500 with malformed token', done => {
        TestCore.axios(fixtures.authToken.malformed).get('/user')
          .then(response => TestCore.rejectHttpStatus(response.status, done))
          .catch(data => TestCore.assertResponseStatus(500, data, done))
      })
    })

    describe('POST /login', () => {
      // Init the default fields object.
      let body = {
        email: config.TEST_USER_EMAIL,
        password: config.TEST_USER_PASS
      }
      // Init the auth fields array.
      let authFields = _.keys(body)
      // Loop through every field.
      for (let i in authFields) {
        if (authFields.hasOwnProperty(i)) {
          // Get the field name.
          let field = authFields[i]
          // Check required field.
          it(`should return status 400 when no "${field}"`, done => {
            TestCore.axios().post('/login', _.omit(body, [field]))
              .then(response => TestCore.rejectHttpStatus(response.status, done))
              .catch(data => TestCore.assertResponseStatus(400, data, done))
          })
          // Check wrong type.
          it(`should return status 400 when wrong type for field "${field}"`, done => {
            TestCore.axios().post('/login', { ...body, [field]: fixtures[field].wrongType })
              .then(response => TestCore.rejectHttpStatus(response.status, done))
              .catch(data => TestCore.assertResponseStatus(400, data, done))
          })
          // Check malformed value.
          it(`should return status 400 when malformed "${field}"`, done => {
            TestCore.axios().post('/login', { ...body, [field]: fixtures[field].malformed })
              .then(response => TestCore.rejectHttpStatus(response.status, done))
              .catch(data => TestCore.assertResponseStatus(400, data, done))
          })
          // Check existent values.
          it(`should return status 403 when unexisting "${field}"`, done => {
            TestCore.axios().post('/login', { ...body, [field]: fixtures[field].nonExistent })
              .then(response => TestCore.rejectHttpStatus(response.status, done))
              .catch(data => TestCore.assertResponseStatus(403, data, done))
          })
        }
      }
      // Good fields should return a 200 OK.
      it(`should return status 200 when good auth fields`, done => {
        TestCore.axios().post('/login', body)
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('email')
              expect(response.data).to.have.property('token')
              expect(response.data).to.have.property('expiresIn')
              expect(response.data.expiresIn).to.be.a('number')
              // Save token once all expectations have been fulfilled.
              token = response.data.token
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })

    describe('GET /user', () => {
      it('should return status 200 when good token', done => {
        TestCore.axios(token).get('/user')
          .then(response => {
            try {
              expect(response.status).to.equal(200)
              expect(response.data).to.have.property('_id')
              expect(response.data).to.have.property('email')
              expect(response.data).to.have.property('role')
              expect(response.data).to.have.property('termsAccepted')
              expect(response.data).to.have.property('createdAt')
              expect(response.data).to.have.property('updatedAt')
              token = null
              done()
            } catch (error) {
              done(error)
            }
          })
          .catch(done)
      })
    })
  }
}

// Export.
module.exports = AuthTests
