// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const axios = require('axios')
const { expect } = require('chai')

// =============================================================================
// Main.
// =============================================================================

// Init the Test class.
class TestCore {
  static axios (token) {
    // Init the instance.
    let axiosInstance = axios.create({
      baseURL: `${config.SERVER_URL}`
    })
    // If token, also set header.
    if (token) {
      axiosInstance.defaults.headers.common['X-Access-Token'] = token
    } else {
      delete axiosInstance.defaults.headers.common['X-Access-Token']
    }
    // Return axios instance.
    return axiosInstance
  }

  // Assert a response status.
  static assertResponseStatus (status, data, done) {
    try {
      expect(data.response.status).to.equal(status)
      done()
    } catch (error) {
      done(error)
    }
  }

  // Reject a test if a response returned a wrong response status.
  static rejectHttpStatus (status, done) {
    done(new Error(`returned status ${status}`))
  }
}

// Export.
module.exports = TestCore
