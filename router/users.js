// =============================================================================
// Dependencies.
// =============================================================================

// Controllers.
const controllers = require('../controllers')

// Middleware.
const validate = require('../middleware/validate')
const auth = require('../middleware/auth')

// =============================================================================
// Main.
// =============================================================================

// Declare routes.
const load = app => {
  // Get information about the authenticated user in JSON format.
  app.get('/user', auth.require.token.asReqHeader, controllers.users.me)
  // Create a user. Returns the created user in JSON format.
  app.post('/user', validate.body.User.signup, controllers.users.create, controllers.auth.login)
  // Set a user in recovery state. Returns the updated user in JSON format.
  app.post('/recovery', validate.body.User.preRecovery, controllers.users.setRecovery)
  // Reset the password of a user. Returns the updated user in JSON format.
  app.post('/recover', validate.body.User.postRecovery, controllers.users.resetPassword)
}

// Export.
module.exports = {
  load: load
}
