// =============================================================================
// Dependencies.
// =============================================================================

// Controllers.
const controllers = require('../controllers')

// Middleware.
const validate = require('../middleware/validate')
const auth = require('../middleware/auth')

// =============================================================================
// Main.
// =============================================================================

// Declare routes.
const load = app => {
  // Returns a list of projects from the authenticated user in JSON format.
  app.get('/projects', auth.require.token.asReqHeader, controllers.projects.user)
  // Create a project. Returns the created project in JSON format.
  app.post('/project', auth.require.token.asReqHeader, validate.body.Project.all, controllers.projects.create)
  // Get the information about a project in JSON format.
  app.get('/project/:id', auth.require.token.asReqHeader, validate.params.id, controllers.projects.read)
  // Update the information of an existing project. Returns the updated project in JSON format.
  app.patch('/project/:id', auth.require.token.asReqHeader, validate.params.id, validate.body.Project.all, controllers.projects.update)
  // Delete an existing project. Returns an empty response.
  app.delete('/project/:id', auth.require.token.asReqHeader, validate.params.id, controllers.projects.delete)
  // Generate project sheets as PDF. Returns the url of the resulting file in a JSON object, inside attribute 'url'.
  app.post('/pdf/:id', auth.require.token.asReqHeader, validate.params.id, validate.body.Project.id, controllers.projects.generatePDF)
  // Download the generated PDF of a project.
  app.get('/pdf/:id', auth.require.token.asQueryParam, validate.params.id, controllers.projects.downloadPDF)
  // Generate project audio in WAV format. Returns the url of the resulting file in a JSON object, inside attribute 'file'.
  app.post('/wav/:id', auth.require.token.asReqHeader, validate.params.id, validate.body.Project.id, controllers.projects.generateWAV)
  // Download the generated WAV file of a project.
  app.get('/wav/:id', auth.require.token.asQueryParam, validate.params.id, controllers.projects.downloadWAV)
  // Generate project audio in MP3 format. Returns the url of the resulting file in a JSON object, inside attribute 'file'.
  app.post('/mp3/:id', auth.require.token.asReqHeader, validate.params.id, validate.body.Project.id, controllers.projects.generateMP3)
  // Download the generated MP3 file of a project.
  app.get('/mp3/:id', auth.require.token.asQueryParam, validate.params.id, controllers.projects.downloadMP3)
}

// Export.
module.exports = {
  load: load
}
