// =============================================================================
// Dependencies.
// =============================================================================

// Controllers.
const controllers = require('../controllers')

// Middleware.
const auth = require('../middleware/auth')

// =============================================================================
// Main.
// =============================================================================

// Declare routes.
const load = app => {
  app.all('/admin/*', auth.require.admin.asReqHeader)
  app.get('/admin/users', controllers.users.all)
  app.get('/admin/projects', controllers.projects.all)
}

// Export.
module.exports = {
  load: load
}
