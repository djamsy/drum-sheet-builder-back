// =============================================================================
// Dependencies.
// =============================================================================

// Controllers.
const controllers = require('../controllers')

// =============================================================================
// Main.
// =============================================================================

// Declare routes.
const load = app => {
  // Returns a JSON object with information about the project.
  app.get('/', controllers.info)
}

// Export.
module.exports = {
  load: load
}
