// =============================================================================
// Dependencies.
// =============================================================================

// Controllers.
const controllers = require('../controllers')

// Middleware.
const validate = require('../middleware/validate')

// =============================================================================
// Main.
// =============================================================================

// Declare routes.
const load = app => {
  // Authenticates a user. Returns a JSON object containing information about the user and a signed JWT.
  app.post('/login', validate.body.User.login, controllers.auth.verify, controllers.auth.login)
}

// Export.
module.exports = {
  load: load
}
