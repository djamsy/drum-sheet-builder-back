// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const path = require('path')
const glob = require('glob')
const util = require('util')

// Controllers.
const controllers = require('../controllers')

// Rate limiter.
const limiter = require('../utils/limiter')

// =============================================================================
// Main.
// =============================================================================

// Promisify glob.
const find = util.promisify(glob)

// Load function.
const load = (app) => {
  return new Promise((resolve, reject) => {
    // Find all .js files in same folder.
    find(path.join(__dirname, '*.js'))
      .then(files => {
        // Init.
        app.all('*', controllers.init, limiter(config.MAX_CALLS_PER_MINUTE))
        // Require every file and perform the load function.
        files.forEach(file => {
          if (path.basename(file) !== 'index.js') {
            require(file).load(app)
          }
        })
        // Default fallback.
        app.all('*', controllers.fallback)
        resolve()
      })
      .catch(error => {
        reject(error)
      })
  })
}

// Export.
module.exports = {
  load: load
}
