module.exports = {
  SERVER_PORT: 3001,
  SERVER_URL: 'https://api.drumsheetbuilder.com',
  LOG_LEVEL: 'info', /* Winston logger */
  LOGGER_FORMAT: 'combined', /* Morgan logger */
  TOKEN_EXPIRATION: 604800,
  FRONTEND_URL: ['https://drumsheetbuilder.com', 'https://www.drumsheetbuilder.com'],
  MAX_CALLS_PER_MINUTE: 60,
  RECOVERY_TOKEN_LENGTH: 32,
  EMAIL_SERVICE: 'gmail'
}
