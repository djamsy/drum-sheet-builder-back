module.exports = {
  SERVER_PORT: 3001,
  SERVER_URL: 'http://localhost:3001',
  LOG_LEVEL: 'silly', /* Winston logger */
  LOGGER_FORMAT: 'dev', /* Morgan logger */
  TOKEN_EXPIRATION: 604800, /* a week */
  FRONTEND_URL: 'http://localhost:3000',
  MAX_CALLS_PER_MINUTE: 0 /* no limit */,
  RECOVERY_TOKEN_LENGTH: 32,
  EMAIL_SERVICE: 'gmail'
}
