# DrumSheetBuilder API Documentation

Version 1.10.2. Last update: 2019-06-12. Generated automatically from project code.

### POST /login

Authenticates a user. Returns a JSON object containing information about the user and a signed JWT.

#### Authentication

None.

#### Validation

The request body must contain the following parameters:

- `email` (String): **required**.

- `password` (String): **required**.

### GET /

Returns a JSON object with information about the project.

#### Authentication

None.

#### Validation

None.

### GET /projects

Returns a list of projects from the authenticated user in JSON format.

#### Authentication

**Authenticated user**.

#### Validation

None.

### POST /project

Create a project. Returns the created project in JSON format.

#### Authentication

**Authenticated user**.

#### Validation

The request body must contain the following parameters:

- `title` (String): **required**.

- `timeSignature` (String): **required**.

- `bpm` (Number): **required**.

- `lines` (Object): **required**.

- `instrumentOrder` (String): **required**.

- `instrumentIcons` (String): **required**.

- `showGrid` (Boolean): optional.

### GET /project/:id

Get the information about a project in JSON format.

#### Authentication

**Authenticated user**.

#### Validation

None.

### PATCH /project/:id

Update the information of an existing project. Returns the updated project in JSON format.

#### Authentication

**Authenticated user**.

#### Validation

The request body must contain the following parameters:

- `title` (String): **required**.

- `timeSignature` (String): **required**.

- `bpm` (Number): **required**.

- `lines` (Object): **required**.

- `instrumentOrder` (String): **required**.

- `instrumentIcons` (String): **required**.

- `showGrid` (Boolean): optional.

### DELETE /project/:id

Delete an existing project. Returns an empty response.

#### Authentication

**Authenticated user**.

#### Validation

None.

### POST /pdf/:id

Generate project sheets as PDF. Returns the url of the resulting file in a JSON object, inside attribute 'url'.

#### Authentication

**Authenticated user**.

#### Validation

None.

### GET /pdf/:id

Download the generated PDF of a project.

#### Authentication

**Authenticated user**.

#### Validation

None.

### POST /wav/:id

Generate project audio in WAV format. Returns the url of the resulting file in a JSON object, inside attribute 'file'.

#### Authentication

**Authenticated user**.

#### Validation

None.

### GET /wav/:id

Download the generated WAV file of a project.

#### Authentication

**Authenticated user**.

#### Validation

None.

### POST /mp3/:id

Generate project audio in MP3 format. Returns the url of the resulting file in a JSON object, inside attribute 'file'.

#### Authentication

**Authenticated user**.

#### Validation

None.

### GET /mp3/:id

Download the generated MP3 file of a project.

#### Authentication

**Authenticated user**.

#### Validation

None.

### GET /user

Get information about the authenticated user in JSON format.

#### Authentication

**Authenticated user**.

#### Validation

None.

### POST /user

Create a user. Returns the created user in JSON format.

#### Authentication

None.

#### Validation

The request body must contain the following parameters:

- `email` (String): **required**.

- `password` (String): **required**.

- `termsAccepted` (Boolean): **required**.

- `role` (String): optional.

### POST /recovery

Set a user in recovery state. Returns the updated user in JSON format.

#### Authentication

None.

#### Validation

The request body must contain the following parameters:

- `email` (String): **required**.

### POST /recover

Reset the password of a user. Returns the updated user in JSON format.

#### Authentication

None.

#### Validation

The request body must contain the following parameters:

- `recoveryToken` (String): optional.

- `email` (String): **required**.

- `password` (String): **required**.

