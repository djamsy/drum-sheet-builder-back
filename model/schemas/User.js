// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const mongoose = require('mongoose')

// Custom validator.
const validator = require('../../utils/validator')

// =============================================================================
// Schema definition.
// =============================================================================

// Define schema.
const schema = new mongoose.Schema({
  email: {
    ...validator.User.email
  },
  password: {
    ...validator.User.password,
    maxlength: 64
  },
  termsAccepted: {
    ...validator.User.termsAccepted
  },
  role: {
    ...validator.User.role
  },
  recoveryToken: {
    ...validator.User.recoveryToken
  },
  createdAt: {
    type: Date,
    required: false,
    default: new Date()
  },
  updatedAt: {
    type: Date,
    required: false,
    default: new Date()
  }
})

// Always set update date before save.
schema.pre('save', function (next) {
  this.updatedAt = new Date()
  return next()
})

// Export.
module.exports = schema
