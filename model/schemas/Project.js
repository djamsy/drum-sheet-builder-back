// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const mongoose = require('mongoose')

// Custom validator.
const validator = require('../../utils/validator')

// =============================================================================
// Schema definition.
// =============================================================================

// Define schema.
const schema = new mongoose.Schema({
  title: {
    ...validator.Project.title
  },
  userId: {
    ...validator.Project.userId
  },
  timeSignature: {
    ...validator.Project.timeSignature
  },
  bpm: {
    ...validator.Project.bpm
  },
  lines: {
    ...validator.Project.lines
  },
  instrumentOrder: {
    ...validator.Project.instrumentOrder
  },
  instrumentIcons: {
    ...validator.Project.instrumentIcons
  },
  showGrid: {
    ...validator.Project.showGrid
  },
  createdAt: {
    type: Date,
    required: false,
    default: new Date()
  },
  updatedAt: {
    type: Date,
    required: false,
    default: new Date()
  }
})

// Always set update date before save.
schema.pre('save', function (next) {
  this.updatedAt = new Date()
  return next()
})

// Export.
module.exports = schema
