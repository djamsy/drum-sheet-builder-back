// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const mongoose = require('mongoose')

// Schemas.
const schemas = {
  user: require('./schemas/User'),
  project: require('./schemas/Project')
}

// =============================================================================
// Main.
// =============================================================================

// Export all first-level models.
// Don't forget to add the model name to the array of global variables in
// package.json for standard JS (property: "standard").
module.exports = {
  User: mongoose.model('User', schemas.user),
  Project: mongoose.model('Project', schemas.project)
}
