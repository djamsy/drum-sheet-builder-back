'use strict'

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const bodyParser = require('body-parser')
const express = require('express')
const winston = require('winston')
require('winston-daily-rotate-file')
const morgan = require('morgan')
const chalk = require('chalk')
const cors = require('cors')
const http = require('http')
const path = require('path')
const fs = require('fs')

// Router.
const router = require('./router')

// Modules.
const modules = require('./modules')

// Utils.
const fns = require('./utils/functions')

// =============================================================================
// Initialization.
// =============================================================================

// Check for existence of .env file.
try {
  fs.accessSync(path.join(__dirname, '.env'), fs.constants.F_OK)
  require('dotenv').config()
} catch (err) {
  console.log(`${chalk.red('error')} File ${chalk.bold('.env')} not found in ${chalk.underline(__dirname)}.`)
  process.exit(0)
}

// Get config from read file.
const config = fns.loadConfig(process.env.NODE_ENV)

// Configure winston log.
const log = winston.createLogger({
  level: config.LOG_LEVEL,
  transports: [
    new (winston.transports.DailyRotateFile)({
      format: winston.format.combine(
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.json()
      ),
      level: 'info',
      filename: path.join(config.LOGS_PATH, 'api-%DATE%.log'),
      datePattern: 'YYYY-MM-DD',
      zippedArchive: false,
      maxSize: '20m',
      maxFiles: '14d'
    })
  ]
})

// Show logs in console when development environment.
if (process.env.NODE_ENV === 'development') {
  log.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.simple()
    )
  }))
}

// Create express app.
let app = express()

// Create HTTP server.
let server = http.createServer(app)

// =============================================================================
// App configuration.
// =============================================================================

// Use morgan.
app.use(morgan(config.LOGGER_FORMAT))

// Rewrite remote address to the one passed by Nginx in a header.
morgan.token('remote-addr', req => req.get['X-Real-IP'] || req.ip)

// Static files URL.
app.use('/assets', express.static('public'))

// Parse application/json and application/x-www-form-urlencoded.
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// Enable CORS.
app.use(cors({
  origin: config.FRONTEND_URL,
  optionsSuccessStatus: 200
}))

// Set app port after loading config.
app.set('port', config.SERVER_PORT)

// Load extra configuration.
fns.loadExtraConfiguration(config)

// Export global variables.
global.config = config
global.log = log

// Build not included directories.
fns.mkdirs([
  path.join(__dirname, 'files'),
  path.join(__dirname, 'files/pdf'),
  path.join(__dirname, 'files/wav'),
  path.join(__dirname, 'files/mp3')
])
  .then(() => {
    // Load modules.
    modules.load()
      .then(() => {
        // Load routes.
        router.load(app)
          .then(() => {
            // Listen on provided port, on all network interfaces.
            server.listen(config.SERVER_PORT)
            server.on('error', function (error) {
              return fns.onServerError(error, config.SERVER_PORT)
            })
            server.on('listening', function () {
              return fns.onServerListening(server)
            })
            // Catch the SIGINT and SIGTERM events and destroy modules safely.
            process.on('SIGINT', fns.onServerShutdown(modules))
            process.on('SIGTERM', fns.onServerShutdown(modules))
          })
          .catch(error => {
            log.error(`Error loading routes: ${fns.stringifyError(error)}.`)
            process.exit(1)
          })
      })
      .catch(error => {
        log.error(`Error loading modules: ${fns.stringifyError(error)}.`)
        process.exit(1)
      })
  })
  .catch(error => {
    log.error(`Error creating directories: ${fns.stringifyError(error)}.`)
    process.exit(1)
  })
