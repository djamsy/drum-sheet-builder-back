// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const sanitizeFileName = require('sanitize-filename')

// =============================================================================
// Constants.
// =============================================================================

// Regular expression for <script> tags and their content.
// This regular expression is based on the one used by jQuery.
const SCRIPT_TAG_REGEX = /<\s*script\b[^<]*(?:(?!<\s*\/\s*script\s*>)<[^<]*)*<\s*\/\s*script\s*>/gi

// =============================================================================
// Main.
// =============================================================================

// Export.
module.exports = {
  html: {
    // Convert < and > to &lt; and &gt;.
    escape: str => str.replace(/</g, '&lt;').replace(/>/gi, '&gt;'),
    // Remove HTML tags, but not their content, from a string.
    removeTags: str => str.replace(/<(?:.|\n)*?>/gm, ''),
    // Remove <script> tags and every content inside them from a string.
    removeJs: str => str.replace(SCRIPT_TAG_REGEX, '')
  },
  number: {
    // Convert any number to an int.
    toInt: num => num ? Math.round(num) : null
  },
  // Ensure a parameter is a boolean.
  boolean: bool => bool + '' === 'true',
  // Don't allow some reserved characters.
  fileName: name => sanitizeFileName(name)
}
