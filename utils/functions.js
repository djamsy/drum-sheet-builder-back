// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

// Errors.
const errors = require('../utils/errors')

// Utils.
const customValidator = require('./validator')

// Datasets.
const instruments = require('../datasets/instruments').asArray
const constants = require('../datasets/constants')

// =============================================================================
// Main.
// =============================================================================

// Init the functions object.
let fns = {}

// Compare Mongo Object IDs after converting them to string.
fns.equalObjectIds = (id1, id2) => id1.toString() === id2.toString()

// Load extra configuration.
fns.loadExtraConfiguration = (config) => {
  // Information from project.
  config.project = require('../package.json')
}

// Load config from an environment.
fns.loadConfig = env => {
  // If no environment, set default.
  if (!env || !customValidator.config.NODE_ENV.validate.validator(env)) {
    process.env.NODE_ENV = customValidator.config.NODE_ENV.default
    console.log(`${chalk.red('error')} Wrong NODE_ENV. Setting to default: ${process.env.NODE_ENV}.`)
  }
  // Require the configuration for that environment.
  let envConfig = require(path.join(__dirname, '..', 'conf', `config.${process.env.NODE_ENV}.js`))
  // Append the process.env variables to it.
  let config = {
    ...envConfig,
    ...process.env
  }
  // Find errors.
  let validationErrors = fns.getErrors('config', config)
  // If errors, print and then exit process.
  if (validationErrors) {
    validationErrors.forEach(error => console.log(`${chalk.red('error')} Environment error: ${error}.`))
    process.exit(1)
  }
  // Return.
  return config
}

// Get the error depending on the code returned by Mongoose.
fns.getMongooseError = error => {
  switch (error.name) {
    case 'ValidationError': return errors.VALIDATION_ERROR
    default: return errors.QUERY_ERROR
  }
}

// Get the error depending on the code returned by jsonwebtoken.
fns.getJWTError = error => {
  switch (error.name) {
    case 'TokenExpiredError': return errors.TOKEN_EXPIRED
    case 'JsonWebTokenError': return errors.WRONG_TOKEN
    default: return errors.UNKNOWN_TOKEN_ERROR
  }
}

// Get the error

// Implementation of 'instance of' for converting between types.
fns.instanceOf = (thing, instance) => {
  switch (instance.name) {
    case 'String': return _.isString(thing)
    case 'Number': return _.isNumber(thing)
    case 'Boolean': return _.isBoolean(thing)
    case 'Array': return _.isArray(thing)
    case 'Object': return _.isObject(thing)
    case 'Date': return _.isDate(thing)
    default: return true
  }
}

// Validate all the fields from a specific model.
fns.getErrors = (model, obj, options) => {
  // Init the errors array.
  let errors = []
  // Init the validator object.
  let validatorObject = customValidator[model]
  // If options.fieldsToValidate is specified, pick those.
  if (options && options.fieldsToValidate) {
    validatorObject = _.pick(validatorObject, options.fieldsToValidate)
  }
  // If options.fieldsToOmit is specified, omit those.
  if (options && options.fieldsToOmit) {
    validatorObject = _.omit(validatorObject, options.fieldsToOmit)
  }
  // For every field to validate.
  for (let field in validatorObject) {
    if (validatorObject.hasOwnProperty(field)) {
      // Check if required field.
      if (validatorObject[field].required && obj[field] === undefined) {
        errors.push(`${field} is a required field`)
      } else {
        if (obj[field] !== undefined) {
          // Check type.
          if (fns.instanceOf(obj[field], validatorObject[field].type)) {
            // Validate field.
            if (validatorObject[field].validate) {
              if (!validatorObject[field].validate.validator(obj[field])) {
                errors.push(validatorObject[field].validate.message({
                  value: obj[field]
                }))
              }
            }
          } else {
            errors.push(`${field} must be a ${validatorObject[field].type.name}`)
          }
        }
      }
    }
  }
  // Return the array of errors, or null if none found.
  return errors.length ? errors : null
}

// Event listener for HTTP server "listening" event.
// Called when server is listening.
fns.onServerListening = server => {
  log.info(`Listening on port ${server.address().port}.`)
}

// Event listener for HTTP server "error" event.
// Called when server attempts to start listening, but fails.
fns.onServerError = (error, port) => {
  if (error.syscall && error.syscall !== 'listen') throw error
  let bind = typeof port === 'string' ? `Pipe ${port}` + port : `Port ${port}`
  // Handle specific listen errors with friendly messages.
  switch (error.code) {
    case 'EACCES':
      log.error(`${bind} requires elevated privileges.`)
      return process.exit(1)
    case 'EADDRINUSE':
      log.error(`${bind} is already in use.`)
      return process.exit(1)
    default:
      throw error
  }
}

// Event listener for HTTP server shutdown.
// Called when SIGTERM or SIGINT events caught on process.
fns.onServerShutdown = modules => {
  return () => {
    modules.destroy()
      .then(() => process.exit(1))
  }
}

// Stringify error if necessary.
fns.stringifyError = error => {
  if (error instanceof Error) {
    return JSON.stringify({ name: error.name, message: error.message })
  } else if (_.isObject(error)) {
    return JSON.stringify(error)
  } else {
    return error.toString()
  }
}

// Build directories if not existing.
fns.mkdirs = dirs => {
  return new Promise((resolve, reject) => {
    // Create all the directories, asynchronously.
    for (let i = 0; i < dirs.length; i++) {
      try {
        // Check if exists.
        fs.accessSync(dirs[i], fs.constants.F_OK)
      } catch (error) {
        // If the error occurs because the directory doesn't exist, create it.
        if (error.code === 'ENOENT') {
          // Create the directory.
          fs.mkdirSync(dirs[i])
          log.info(`Creating unexisting directory ${dirs[i]}.`)
        // Otherwise, reject.
        } else {
          reject(error)
        }
      }
    }
    // Resolve.
    resolve()
  })
}

// Get the location of a PDF file.
fns.getPdfFileLocation = name => path.resolve(__dirname, `../files/pdf/${name}.pdf`)

// Get the location of a WAV file.
fns.getWavFileLocation = name => path.resolve(__dirname, `../files/wav/${name}.wav`)

// Get the location of an MP3 file.
fns.getMp3FileLocation = name => path.resolve(__dirname, `../files/mp3/${name}.mp3`)

// Get the location of a template file.
fns.getTemplateFileLocation = (type, name) => path.resolve(__dirname, `../templates/${type}/${name}/template.ejs`)

// Get the location of a template style.
fns.getTemplateStyleLocation = (type, name) => path.resolve(__dirname, `../templates/${type}/${name}/style.ejs`)

// Check if a file exists.
fns.fileExists = fileName => {
  return new Promise((resolve, reject) => {
    // Try to access file.
    fs.access(fileName, fs.constants.F_OK, error => {
      if (error) reject(error)
      else resolve(fileName)
    })
  })
}

// Ordered labels of the instruments.
fns.getOrderedInstrumentLabels = orderType => {
  // Init the result.
  let r = {}
  // Loop through every instrument.
  for (let i in instruments) {
    if (instruments.hasOwnProperty(i)) {
      r[instruments[i].label] = instruments[i].order[orderType]
    }
  }
  // Return.
  return r
}

// Order the instruments of a line.
fns.orderInstruments = (orderType, bar) => {
  // Get the ordered instrument labels.
  let orderedInstruments = _.orderBy(instruments, [`${orderType}Order`], ['asc'])
  // Init result.
  let r = {}
  // Create objects of .
  for (let i in orderedInstruments) {
    if (orderedInstruments.hasOwnProperty(i)) {
      let label = orderedInstruments[i].label
      // Push to result.
      if (bar[label]) {
        r[label] = bar[label]
      }
    }
  }
  // Return.
  return r
}

// Returns true if the string is a valid object ID.
fns.isObjectId = str => /^[0-9a-fA-F]{24}$/.test(str)

// Returns true if a number is a power of 2.
fns.isPowerOf2 = x => (Math.log(x) / Math.log(2)) % 1 === 0

// Returns true if a number is multiplier of another.
fns.isMultiplierOf = (x, n) => n % x === 0

// Returns true if a number is multiplier of 3.
fns.isMultiplierOf3 = n => fns.isMultiplierOf(3, n)

// Check if a number is prime.
fns.isPrime = num => {
  for (let i = 2; i < num; i++) {
    if (num % i === 0) return false
  }
  return num !== 1 && num !== 0
}

// Get the maximum prime divisor of a number.
fns.getMaxPrimeDivider = beats => {
  // If number is prime, return.
  if (fns.isPrime(beats)) return beats
  // Get the prime dividers.
  let primeDividers = _.times(Math.floor(beats / 2), Number)
    .map(n => n + 1)
    .filter(n => fns.isPrime(n) && beats % n === 0)
  // Return the maximum.
  return (primeDividers.length ? _.max(primeDividers) : beats)
}

// Get the cell border.
fns.getCellBorder = (beatIdx, timeSignature, showGrid) => {
  // Find the maximum prime divisor.
  let maxPrimeDivider = fns.getMaxPrimeDivider(constants.timeSignatures[timeSignature].beats)
  // Change the right border accordingly, depending on beatIdx.
  if (!showGrid || beatIdx === 0 || (constants.timeSignatures[timeSignature].disabledBeats && constants.timeSignatures[timeSignature].disabledBeats.indexOf(beatIdx) !== -1)) {
    return '1px solid #fff'
  } else {
    if (beatIdx % maxPrimeDivider === 0) return '1px solid #ccc'
    else return '1px solid #fff'
  }
}

// Calculate the width of each instrument of the line to make it spread to the end of the DOM.
fns.calculateInstrumentSize = (beats, bars) => {
  // Calculate the reserved space.
  let availableSpace = constants.maxLineWidth -
    35 /* instrument label */ -
    bars * beats /* right border of each cell */
  // Substract the available space to the maximum line width and divide by beats * bars.
  let instrumentSize = Math.floor(availableSpace / (bars * beats))
  // Return.
  return Math.min(instrumentSize, constants.maxInstrumentSize)
}

// Generate an alphanumeric string of a desired length.
fns.generateToken = () => {
  // Init result.
  let r = ''
  // Append a new character to result, 'len' times.
  for (let i = 0; i < config.RECOVERY_TOKEN_LENGTH; i++) {
    r += constants.customTokenCharset[Math.floor(Math.random() * constants.customTokenCharset.length)]
  }
  // Return result.
  return r
}

// Get the front-end URL.
fns.getFrontEndUrl = () => {
  return _.isArray(config.FRONTEND_URL) ? config.FRONTEND_URL[0] : config.FRONTEND_URL
}

// Get the URL of the assets.
fns.getAssetsUrl = () => {
  return `${config.SERVER_URL}/assets`
}

// Returns an instrument hit from the instrument label and the hit label.
fns.getHit = (instrumentLabel, hitLabel) => {
  // Get the instrument.
  let instrument = _.find(instruments, { label: instrumentLabel })
  // Return the hit.
  return _.find(instrument.hits, { label: hitLabel })
}

// Capitalize first letter of a word.
fns.ucFirst = str => str.charAt(0).toUpperCase() + str.slice(1)

// Get own URLs.
fns.getOwnUrls = () => {
  let urls = [config.SERVER_URL]
  if (_.isArray(config.FRONTEND_URL)) return urls.concat(config.FRONTEND_URL)
  urls.push(config.FRONTEND_URL)
  return urls
}

// Export.
module.exports = fns
