/**
 * Rate limiter based on the 'limiter' package provided by NPM.
 * This file is a definition of our custom rate limiter, converted to
 * middleware, to be used before every request.
 */

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const RateLimiter = require('limiter').RateLimiter

// Errors.
const errors = require('./errors')

// =============================================================================
// Main.
// =============================================================================

// Export as a function depending on the rate limit.
module.exports = rateLimit => {
  // Initialize limiter.
  const limiter = new RateLimiter(rateLimit, 'minute')
  // Return middleware function.
  return (req, res, next) => {
    // If the limit is 0, return.
    if (rateLimit === 0) return next()
    // Send 429 header to client when rate limiting is in effect.
    limiter.removeTokens(1, (error, remainingRequests) => {
      // If error, return.
      if (error) {
        log.error(`Error when using rate limiter: ${JSON.stringify(error)}.`)
        return res.api.error(errors.UNKNONW_ERROR)
      }
      // Get the request IP.
      let requestIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress
      // Check for number of requests.
      if (remainingRequests < 1) {
        log.error(`Too many ${req.method} ${req.path} requests from IP ${requestIP}.`)
        return res.api.error(errors.TOO_MANY_REQUESTS)
      }
      return next()
    })
  }
}
