// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const util = require('util')

// =============================================================================
// Error definition.
// =============================================================================

/**
 * Error formatting:
 * @property status:  HTTP status code.
 * @property message: internal error message. Only lowercase words and _.
 */
const errors = {
  // Client errors.
  NOT_FOUND: { status: 404, message: 'Not found' },
  // Server errors.
  UNKNONW_ERROR: { status: 500, message: 'Unknown server error' },
  QUERY_ERROR: { status: 500, message: 'Query error' },
  PDF_RENDER_ERROR: { status: 500, message: 'Error rendering sheets' },
  WAV_RENDER_ERROR: { status: 500, message: 'Error rendering audio' },
  AUDIO_CONVERSION_ERROR: { status: 500, message: 'Audio conversion error' },
  // Request body/query/params errors.
  REQUIRED_PARAMS: { status: 422, message: 'Missing required parameters' },
  VALIDATION_ERROR: { status: 400, message: 'Validation error' },
  WRONG_REQ_PARAM: { status: 400, message: 'Invalid request parameter' },
  // Authentication errors.
  EMAIL_EXISTS: { status: 403, message: 'E-mail %s already registered' },
  UNEXISTING_EMAIL: { status: 403, message: 'User %s is not registered' },
  WRONG_AUTH: { status: 403, message: 'Wrong e-mail/password combination' },
  NO_TOKEN: { status: 401, message: 'No web token provided' },
  TOKEN_EXPIRED: { status: 401, message: 'Token expired' },
  WRONG_TOKEN: { status: 500, message: 'Wrong web token' },
  UNKNOWN_TOKEN_ERROR: { status: 500, message: 'Failed to authenticate token' },
  NOT_AUTHORIZED: { status: 403, message: 'Unauthorized access' }, /* not the owner */
  INSUFFICIENT_PRIVILEGES: { status: 403, message: 'Insufficient privileges' }, /* not admin */
  USER_NOT_FOUND: { status: 401, message: 'User %s not found' },
  RECOVERY_STATE: { status: 403, message: 'Please, reset your password with the token that was sent to your mailbox' },
  WRONG_RECOVERY_TOKEN: { status: 401, message: 'Wrong recovery token' },
  NO_RECOVERY_TOKEN: { status: 401, message: 'No recovery token provided' },
  CROSS_SITE: { status: 403, message: 'Cross-site usage of authentication token detected' },
  // Mail errors.
  MAIL_ERROR: { status: 500, message: 'Error sending mail to %s' },
  // Other errors/cybersecurity guards.
  TOO_MANY_REQUESTS: { status: 429, message: 'Too many requests' }
}

// =============================================================================
// Anything else.
// =============================================================================

// Function to format an error message.
const format = (error) => {
  return formats => {
    // Convert formats to array, if necessary.
    formats = typeof formats === 'object' ? formats : [formats]
    // Format the error message and return.
    return {
      ...error,
      message: util.format.apply(null, [error.message].concat(formats))
    }
  }
}

// Create .format function for every error.
for (let key in errors) {
  errors[key].format = format(errors[key])
}

// Export.
module.exports = {
  ...errors,
  format: format
}
