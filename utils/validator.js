// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const fs = require('fs')
const validator = require('validator')

// Datasets.
const constants = require('../datasets/constants')
const instruments = require('../datasets/instruments').asObject

// =============================================================================
// Aux. validation functions.
// =============================================================================

// Validate a directory name.
const existingDirectory = {
  validator: val => fs.existsSync(val),
  message: props => `Directory ${props.value} does not exist`
}

// Validate the attributes of a line.
const validateLineAttributes = line => {
  // Init result.
  let result = true
  // Find errors in attribute 'type'.
  result = result && line.type !== undefined
  result = result && _.values(constants.lineTypes).indexOf(line.type) !== -1
  // Find errors in attribute 'bgColor'.
  result = result && line.bgColor !== undefined
  result = result && (line.type === constants.lineTypes.line
    ? _.values(constants.lineBgColors).map(e => e.background).indexOf(line.bgColor) !== -1
    : _.values(constants.separatorBgColors).indexOf(line.bgColor) !== -1)
  // Find errors in attribute 'collapsed'.
  result = result && line.collapsed !== undefined
  result = result && _.isBoolean(line.collapsed)
  // Find errors in attribute 'content'.
  result = result && line.content !== undefined
  result = result && (line.type === constants.lineTypes.line
    ? validateLineContent(line.content)
    : _.isString(line.content))
  // Return the result.
  return result
}

// Validate line content.
const validateLineContent = lineContent => {
  // Check if line is not empty.
  if (_.isEmpty(lineContent)) return false
  // Loop through every bar.
  for (let j = 0; j < lineContent.length; j++) {
    let bar = lineContent[j]
    // Check if bar is a not empty object.
    if (!_.isObject(bar) || _.isEmpty(bar)) return false
    // Check that bar is an array of valid objects.
    for (let instrument in bar) {
      if (bar.hasOwnProperty(instrument)) {
        if (!_.isArray(bar[instrument]) || _.isEmpty(bar[instrument])) return false
        // Check the beat elements.
        for (let k = 0; k < bar[instrument].length; k++) {
          let beat = bar[instrument][k]
          if (!_.isInteger(beat)) return false
          if (beat !== 0 && !instruments[instrument][beat - 1]) return false
        }
      }
    }
  }
  return true
}

// Validate project lines.
const validateLines = {
  validator: val => {
    // If the lines object is empty, return false.
    if (_.isEmpty(val)) return false
    // Loop through each line.
    for (let i = 0; i < val.length; i++) {
      let line = val[i]
      // If a line is not an object, return false.
      if (!_.isObject(line)) return false
      // If a line doesn't have the required attributes, return false.
      if (!validateLineAttributes(line)) return false
    }
    return true
  },
  message: () => `Wrong lines object`
}

// Validate a Gmail e-mail address.
const validateGmailAccount = {
  validator: val => validator.isEmail(val) && /@gmail\.com$/.test(val),
  message: props => `${props.value} is not a valid Gmail account`
}

// Validate a password by checking that its value is not the password placeholder '*****'.
const validatePassword = {
  validator: val => !/^[*]+$/.test(val) && !/[\n\r\s]+/.test(val) && val.trim().length > 0,
  message: () => `Please, provide a password`
}

// =============================================================================
// Model validators.
// =============================================================================

/**
 * Object to define the validation functions to perform when calling API.
 * This means, to manipulate RAW data coming from the request body.
 * This validation may not be the same than the validation performed before an
 * DB insert or update if some fields are modified in between (e.g. password
 * hashing).
 */
const modelValidators = {
  User: {
    email: {
      type: String,
      required: true,
      validate: {
        validator: validator.isEmail,
        message: props => `${props.value} is not a valid e-mail`
      }
    },
    password: {
      type: String,
      required: true,
      validate: {
        validator: val => !/[\n\r\s]+/.test(val) && val.trim().length > 0, /* No whitespaces */
        message: props => `${props.value} is not a valid password`
      }
    },
    termsAccepted: {
      type: Boolean,
      required: true,
      validate: {
        validator: val => val === true,
        message: () => `You must accept the terms and conditions`
      }
    },
    role: {
      type: String,
      required: false,
      default: 'user',
      validate: {
        validator: val => _.includes(['user', 'admin'], val),
        message: props => `${props.value} is not a valid role name`
      }
    },
    recoveryToken: {
      type: String,
      required: false,
      validate: {
        validator: val => (new RegExp(`[a-zA-Z0-9-_]{${config.RECOVERY_TOKEN_LENGTH},${config.RECOVERY_TOKEN_LENGTH}}`)).test(val),
        message: props => `${props.value} is not a valid recovery token`
      }
    }
  },
  Project: {
    title: {
      type: String,
      minlength: 1,
      required: true,
      validate: {
        validator: val => val.trim().length > 0,
        message: props => `${props.value} is not a valid title`
      }
    },
    userId: {
      type: String,
      minlength: 12,
      required: true
    },
    timeSignature: {
      type: String,
      required: true,
      validate: {
        validator: val => _.includes(_.keys(constants.timeSignatures), val),
        message: props => `${props.value} is not a valid time signature`
      }
    },
    bpm: {
      type: Number,
      required: true,
      min: 40,
      max: 220
    },
    lines: {
      type: [],
      required: true,
      validate: validateLines
    },
    instrumentOrder: {
      type: String,
      required: true,
      validate: {
        validator: val => _.includes(_.values(constants.instrumentOrderTypes), val),
        message: props => `${props.value} is not a valid order`
      }
    },
    instrumentIcons: {
      type: String,
      required: true,
      validate: {
        validator: val => _.includes(_.values(constants.instrumentIconTypes), val),
        message: props => `${props.value} is not a valid icon type`
      }
    },
    showGrid: {
      type: Boolean,
      required: false,
      default: true
    }
  }
}

// =============================================================================
// Other validators.
// =============================================================================

// Config valdiator.
const configValidator = {
  NODE_ENV: {
    type: String,
    required: false,
    default: 'development',
    validate: {
      validator: val => _.includes(['production', 'development'], val),
      message: props => `${props.value} is not a valid environment`
    }
  },
  DB_HOST: {
    type: String,
    required: true,
    validate: {
      validator: val => validator.isURL(val, {
        require_protocol: false,
        require_tld: false /* allow localhost */
      }),
      message: props => `${props.value} is not a valid database host`
    }
  },
  DB_PORT: {
    type: String,
    required: true,
    validate: {
      validator: validator.isPort,
      message: props => `${props.value} is not a valid port`
    }
  },
  DB_NAME: {
    type: String,
    required: true
  },
  DB_USER: {
    type: String,
    required: true
  },
  DB_PASS: {
    type: String,
    required: true
  },
  SESSION_SECRET: {
    type: String,
    required: true
  },
  LOGS_PATH: {
    type: String,
    required: true,
    validate: existingDirectory
  },
  EMAIL_USER: {
    type: String,
    required: true,
    validate: validateGmailAccount
  },
  EMAIL_PASSWORD: {
    type: String,
    required: true,
    validate: validatePassword
  },
  TEST_USER_EMAIL: {
    type: String,
    required: true,
    validate: {
      validator: validator.isEmail,
      message: props => `${props.value} is not a valid e-mail`
    }
  },
  TEST_USER_PASS: {
    type: String,
    required: true,
    validate: validatePassword
  }
}

// =============================================================================
// Export.
// =============================================================================

// Export all validators.
module.exports = {
  ...modelValidators,
  config: configValidator
}
