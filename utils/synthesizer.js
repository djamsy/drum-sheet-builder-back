/**
 * An audio synthesizer for JavaScript.
**/

// ============================================================================
// Dependencies.
// ============================================================================

const _ = require('lodash')
const fs = require('fs')
const path = require('path')
const load = require('audio-loader')
const WavEncoder = require('wav-encoder')
const AudioBuffer = require('audio-buffer')
const audioUtils = require('audio-buffer-utils')

// Datasets.
const constants = require('../datasets/constants')
const instruments = require('../datasets/instruments').asArray
const instrumentsObject = require('../datasets/instruments').asObject

// Utils.
const fns = require('./functions')

// ============================================================================
// Constants.
// ============================================================================

// Sample rate of the audio to synthesize.
const DEFAULT_SAMPLE_RATE = 44100

// Number of channels of the audio to synthesize.
const DEFAULT_N_CHANNELS = 2

// ============================================================================
// Class declaration.
// ============================================================================

// Synthesizer class.
class Synthesizer {
  constructor (lib = constants.defaultAudioLib, sampleRate = DEFAULT_SAMPLE_RATE, nChannels = DEFAULT_N_CHANNELS) {
    // Assign class attributes.
    this.lib = lib
    this.sampleRate = sampleRate
    this.nChannels = nChannels
    // Init the audio files object.
    this.audioFiles = {}
    this.initAudiosObject()
    // Init the audios object.
    this.audios = {}
    // Init the synthesizer basic params.
    this.lines = []
    this.hitDepth = null
    this.tbb = null
    this.dob = null
    this.spb = null
    this.nSamples = 0
  }

  // Init the audios object.
  initAudiosObject () {
    // Loop through the instruments object.
    for (let i in instruments) {
      if (instruments.hasOwnProperty(i)) {
        // Init the instrument label.
        let instrumentLabel = instruments[i].label
        // Loop through all the instrument hits.
        for (let j in instruments[i].hits) {
          if (instruments[i].hits.hasOwnProperty(j)) {
            // Init the instrument hit.
            let hitLabel = instruments[i].hits[j].label
            // Push the URL of the audio to load to the audios object.
            this.audioFiles[`${instrumentLabel}-${hitLabel}`] = instruments[i].hits[j].audio[this.lib]
          }
        }
      }
    }
  }

  // Calculate the Time Between Beats (TBB).
  static calculateTBB (bpm) {
    return 60 / +bpm
  }

  // Calculate the hit depth.
  static calculateHitDepth (timeSignature) {
    return +timeSignature.split('/').pop()
  }

  // Calculate the Duration Of a Beat (DOB), in milliseconds.
  static calculateDOB (tbb, hitDepth) {
    return tbb * 1000 / constants.hitDepthFactor[hitDepth]
  }

  // Calculate the number of Samples Per Beat (SPB).
  static calculateSPB (sampleRate, dob) {
    return Math.ceil(sampleRate * dob / 1000)
  }

  // Calculate the total number of samples in a song to synthesize.
  // 'rawLines' is the object that contains all the instrument hits concatenated, not the usual line content.
  static calculateNSamples (spb, rawLines) {
    return Math.ceil(_.head(_.values(rawLines)).length * spb)
  }

  // Get the raw lines content, by extracting all the bars and concatenating them.
  static convertLinesToRaw (lines) {
    // Init the raw lines object.
    let rawLines = {}
    // Loop through every line.
    for (let i = 0; i < lines.length; i++) {
      let line = lines[i]
      // Only if line is not a separator.
      if (line.type === constants.lineTypes.line) {
        // Loop through every bar on the line content.
        for (let j = 0; j < line.content.length; j++) {
          let bar = line.content[j]
          // Loop through every instrument on the bar.
          for (let instrKey in bar) {
            if (bar.hasOwnProperty(instrKey)) {
              // If instrument does not exist in the raw lines object yet, create it.
              if (!rawLines[instrKey]) rawLines[instrKey] = []
              // Concatenate values.
              rawLines[instrKey] = rawLines[instrKey].concat(bar[instrKey])
            }
          }
        }
      }
    }
    // Return the raw lines object.
    return rawLines
  }

  // Load the audio files.
  loadAudios () {
    return new Promise((resolve, reject) => {
      load(this.audioFiles)
        .then(audios => {
          this.audios = audios
          resolve()
        })
        .catch(reject)
    })
  }

  // Init the lines object and calculate derived params.
  init (project) {
    // Init the file name.
    this.fileName = `${project._id}.wav`
    this.filePath = path.resolve(__dirname, `../files/wav/${this.fileName}`)
    // Assign the lines content to class attribute.
    this.lines = project.lines
    // Get the raw lines content, by extracting all the bars and concatenating them.
    this.rawLines = Synthesizer.convertLinesToRaw(this.lines)
    // Calculate the Time Between Beats (TBB).
    this.tbb = Synthesizer.calculateTBB(project.bpm)
    // Calculate the hit depth.
    this.hitDepth = Synthesizer.calculateHitDepth(project.timeSignature)
    // Calculate the Duration Of a Beat (DOB), in milliseconds.
    this.dob = Synthesizer.calculateDOB(this.tbb, this.hitDepth)
    // Calculate the samples per beat.
    this.spb = Synthesizer.calculateSPB(this.sampleRate, this.dob)
    // Calculate the duration of the song, in milliseconds.
    this.nSamples = Synthesizer.calculateNSamples(this.spb, this.rawLines)
  }

  // Get the number of samples of the longest audio.
  getLongestAudioSamples () {
    let longestAudioSamples = 0
    for (let i in this.audios) {
      if (this.audios.hasOwnProperty(i)) {
        if (this.audios[i].length > longestAudioSamples) {
          longestAudioSamples = this.audios[i].length
        }
      }
    }
    return longestAudioSamples
  }

  // Synthesize the lines of a project and save a WAV file with the result.
  synthesize (project) {
    return new Promise((resolve, reject) => {
      // Load the project and derived params.
      this.init(project)
      // Load the audios.
      this.loadAudios()
        .then(() => {
          // Calculate the number of samples of the song, with security guard.
          this.nSamplesExtra = this.nSamples + this.getLongestAudioSamples()
          // Init the synthesized audio buffer.
          let synthesizedAudio = new AudioBuffer({
            length: this.nSamplesExtra,
            sampleRate: this.sampleRate,
            numberOfChannels: this.nChannels
          })
          // Loop through every channel.
          for (let i = 0; i < this.nChannels; i++) {
            // Loop through every instrument.
            for (let instrKey in this.rawLines) {
              if (this.rawLines.hasOwnProperty(instrKey)) {
                let line = this.rawLines[instrKey]
                // Loop through every value of the line.
                for (let k = 0; k < line.length; k++) {
                  // Skip if value is 0.
                  if (line[k] === 0) continue
                  // Get the name of the hit to sum.
                  let hitKey = instrumentsObject[instrKey][line[k] - 1]
                  // Get the signal to be summed.
                  let signalToSum = this.audios[`${instrKey}-${hitKey}`]
                  // Calculate the limit of samples to sum depending on the 'cutPrevious' property of the hit.
                  let samplesToSum = signalToSum.length
                  // Look for the next not null value of the lines array to find out if the hit must cut previous.
                  if (k < line.length - 1) {
                    for (let m = k + 1; m < line.length; m++) {
                      // Break the loop if we reach the limit.
                      if ((m - k) * this.spb > samplesToSum) break
                      // If the next occurrence of this instrument must cut previous, set a sample limit.
                      if (line[m] !== 0 && _.find(instruments, { label: instrKey }).hits[line[m]].cutPrevious) {
                        samplesToSum = (m - k) * this.spb
                      }
                    }
                  }
                  // Sum every sample of the signal.
                  for (let n = 0; n < samplesToSum; n++) {
                    synthesizedAudio._channelData[i][k * this.spb + n] += signalToSum._channelData[i][n]
                  }
                  // If instrument is drag, sum a second occurrence in half the size of a beat.
                  if (fns.getHit(instrKey, hitKey).drag) {
                    for (let n = 0; n < signalToSum.length; n++) {
                      synthesizedAudio._channelData[i][k * this.spb + n + this.spb / 2] += signalToSum._channelData[i][n]
                    }
                  }
                }
              }
            }
          }
          // Trim the audio buffer.
          let synthesizedAudioTrimmed = audioUtils.trim(synthesizedAudio)
          // Normalize the audio file.
          let synthesizedAudioNormalized = audioUtils.normalize(synthesizedAudioTrimmed)
          // Save the audio file.
          this.saveAudioFile(synthesizedAudioNormalized)
            .then(() => resolve(this.filePath))
            .catch(reject)
        })
        .catch(reject)
    })
  }

  // Save an audio file to destination folder.
  saveAudioFile (audioBuffer) {
    return new Promise((resolve, reject) => {
      // Encode the WAV file.
      WavEncoder.encode({
        sampleRate: this.sampleRate,
        channelData: audioBuffer._channelData
      })
        .then(buffer => {
          fs.writeFile(this.filePath, Buffer.from(buffer), error => {
            if (error) return reject(error)
            resolve()
          })
        })
        .catch(reject)
    })
  }
}

// Export.
module.exports = Synthesizer
