// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')

// Errors.
const errors = require('../utils/errors')

// Functions.
const fns = require('../utils/functions')

// User entity.
const User = require('../entities/User')

// =============================================================================
// Main.
// =============================================================================

// Check if a user is authenticated with web token.
const requireToken = source => {
  return (req, res, next) => {
    // Init empty token.
    let token
    switch (source) {
      // Get from query parameters.
      case 'queryParam': token = req.query.token; break
      // Get from the 'X-Access-Token' request header.
      case 'reqHeader': token = req.headers['x-access-token']; break
      // Default, return null.
      default: token = null
    }
    // If no token, return an error.
    if (!token) {
      log.warn(`Intent to ${req.method} ${req.path} with no token.`)
      return res.api.error(errors.NO_TOKEN)
    }
    // Verify the token.
    User.decode(token)
      .then(decodedToken => {
        // Check that the token has all the required properties.
        if (!decodedToken.email || !decodedToken._id) {
          log.warn(`Intent to ${req.method} ${req.path} with a malformed token: ${JSON.stringify(decodedToken)}`)
          return res.api.error(errors.WRONG_TOKEN)
        }
        // Perform the referrer check.
        if (decodedToken.referrer !== '*' && decodedToken.referrer !== req.headers.origin && fns.getOwnUrls().indexOf(decodedToken.referrer) === -1) {
          log.warn(`Intent to ${req.method} ${req.path} with same token but different referrer. Original referrer: ${decodedToken.referrer}. Current origin: ${req.headers.origin}.`)
          return res.api.error(errors.CROSS_SITE)
        }
        // Get the user.
        User.getById(decodedToken._id)
          .then(user => {
            if (!user) {
              log.warn(`User ${decodedToken._id} tried to ${req.method} ${req.path} but does not exist.`)
              return res.api.error(errors.USER_NOT_FOUND.format(decodedToken._id))
            }
            // Return the found user.
            req.user = user
            return next()
          })
          .catch(error => {
            log.error(`Database error when user ${decodedToken.email} being searched by id ${decodedToken._id}: ${JSON.stringify(error)}.`)
            return res.api.error(fns.getMongooseError(error), error.message)
          })
      })
      .catch(error => {
        log.warn(`Intent to ${req.method} ${req.path} with a wrong token: ${JSON.stringify(error)}.`)
        return res.api.error(fns.getJWTError(error), _.omit(error, ['message']))
      })
  }
}

// Check if a user is authenticated with web token and is an admin user.
const requireAdmin = source => {
  return (req, res, next) => {
    requireToken(source)(req, res, () => {
      // Check the user roles.
      if (!req.user.role || req.user.role !== 'admin') {
        log.warn(`User ${req.user.email} tried to ${req.method} ${req.path}, but is not admin.`)
        return res.api.error(errors.INSUFFICIENT_PRIVILEGES)
      }
      return next()
    })
  }
}

// Export.
module.exports = {
  require: {
    token: {
      asReqHeader: requireToken('reqHeader'),
      asQueryParam: requireToken('queryParam')
    },
    admin: {
      asReqHeader: requireAdmin('reqHeader'),
      asQueryParam: requireAdmin('queryParam')
    }
  }
}
