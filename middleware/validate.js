// =============================================================================
// Dependencies.
// =============================================================================

// Utils.
const fns = require('../utils/functions')

// Errors.
const errors = require('../utils/errors')

// =============================================================================
// Main.
// =============================================================================

// Function to validate a request body that defined a model.
// By default, validate all fields.
// If this is note desired, pass an array of fields, options.fieldsToOmit, or
// an array of fields to omit, options.fieldsToOmit.
const validateModelRequestBody = (model, options) => {
  return (req, res, next) => {
    // Validate fields.
    let validationErrors = fns.getErrors(model, req.body, options)
    if (validationErrors) {
      log.warn(`Intent to ${req.method} ${req.path} with malformed body: ${JSON.stringify(validationErrors)}.`)
      return res.api.error(errors.VALIDATION_ERROR, validationErrors)
    }
    next()
  }
}

// Function to validate a parameter from req.params.
const validateParams = paramName => {
  // Init the validation function.
  let validationFunction = null
  // Depending on param, execute validation function.
  switch (paramName) {
    case 'id': validationFunction = fns.isObjectId; break
    default: validationFunction = () => true; break
  }
  // Validate and return consequently.
  return (req, res, next) => {
    // Init the error message.
    let errorMessage = `${req.params[paramName]} is not a valid ${paramName}`
    if (!validationFunction(req.params[paramName])) return res.api.error(errors.WRONG_REQ_PARAM, errorMessage)
    next()
  }
}

// =============================================================================
// Export.
// =============================================================================

// Init the exports object.
module.exports = {
  body: {
    User: {
      signup: validateModelRequestBody('User', { fieldsToOmit: ['recoveryToken'] }),
      login: validateModelRequestBody('User', { fieldsToValidate: ['email', 'password'] }),
      preRecovery: validateModelRequestBody('User', { fieldsToValidate: ['email'] }),
      postRecovery: validateModelRequestBody('User', { fieldsToValidate: ['recoveryToken', 'email', 'password'] })
    },
    Project: {
      all: validateModelRequestBody('Project', { fieldsToOmit: ['userId'] }),
      id: validateModelRequestBody('Project', { fieldsToValidate: ['_id'] })
    }
  },
  params: {
    id: validateParams('id')
  }
}
