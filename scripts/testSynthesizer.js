// A script to test the audio synthesizer.

// =============================================================================
// Dependencies.
// =============================================================================

// ScriptCore.
const ScriptCore = require('./ScriptCore')

// Project entity.
const Project = require('../entities/Project')

// Datasets.
const constants = require('../datasets/constants')

// =============================================================================
// Constants.
// =============================================================================

// Project ID to test.
// const projectIdToTest = '5ca9dbc410e45d4f3b36eeb5' /* Test 2/2 */
// const projectIdToTest = '5cae31b9c5120b18dd6c20e6' /* Test 4/4 */
// const projectIdToTest = '5ca0907f8d77750c600f120f' /* Jazz basic params */
const projectIdToTest = '5cae3962c5120b18dd6c20e7' /* SOAD - Spiders */
// const projectIdToTest = '5caf98498849e327e4153b78' /* Test 6/8 */
// const projectIdToTest = '5caf98c08849e327e4153b7b' /* Test 16/16 */

// =============================================================================
// Main.
// =============================================================================

// Declare the script core object.
const core = new ScriptCore()

// Load modules.
core.modules.load()
  .then(() => {
    // Load the project.
    Project.getById(projectIdToTest)
      .then(project => {
        // Require the synthesizer.
        const Synthesizer = require('../utils/synthesizer')
        // Load the synthesizer object.
        const mySynthesizer = new Synthesizer(constants.defaultAudioLib)
        // Load the audios.
        mySynthesizer.synthesize(project)
          .then(fileName => {
            log.info(`Audio file ${fileName} saved successfully`)
            process.exit(0)
          })
          .catch(error => {
            ScriptCore.logError(`Error loading the audios.`, error)
            process.exit(1)
          })
      })
      .catch(error => {
        ScriptCore.logError(`Error getting the project.`, error)
        process.exit(1)
      })
  })
  .catch(error => {
    ScriptCore.logError(`Error loading the modules.`, error)
    process.exit(1)
  })
