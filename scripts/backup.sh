#!/bin/bash

# Remote backup credentials.
REMOTE_USER=drumsheetbuilder
REMOTE_HOST=thetechnoengineer.com
REMOTE_PATH=/home/$REMOTE_USER/backup

# Declare params.
MAX_BACKUPS=15
COUNT=0
CURDATE=$(date +%s)
DESTINATION=/backup

# Read environment variables.
. ../.env

# Move to destination folder.
cd $DESTINATION

# Execute backup command.
# This will create a backup in folder $DESTINATION/$DB_NAME
mongodump --db $DB_NAME \
    --port $DB_PORT \
    --host $DB_HOST \
    --username $DB_USER \
    --password $DB_PASS \
    --authenticationDatabase admin \
    --out $DESTINATION

# Compress the backup folder.
zip -r $DB_NAME\_$CURDATE.zip $DB_NAME

# Copy to remote.
scp $DB_NAME\_$CURDATE.zip $REMOTE_USER@$REMOTE_HOST:$REMOTE_PATH

# Remove the uncompressed backup folder.
rm -r $DB_NAME

# Remove old dumps
find "$DB_NAME"_*.zip -type f -print0 | sort -rz | tr '\0' '\n' | while read FILENAME; do
    echo $FILENAME
	COUNT=$((COUNT+1))
	if [ $((COUNT)) -gt $((MAX_BACKUPS)) ]; then
		echo "Removing file: $FILENAME"
		rm $FILENAME
		ssh $REMOTE_USER@$REMOTE_HOST rm $REMOTE_PATH/$FILENAME
	fi
done