/**
 * A script for generating the basic API docs in Markdown.
 **/

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')
const fs = require('fs')
const util = require('util')
const path = require('path')
const chalk = require('chalk')
const async = require('async')
const moment = require('moment')

// ScriptCore.
const ScriptCore = require('./ScriptCore')

// Validation middleware.
const validationMwFile = fs.readFileSync(path.resolve(__dirname, '../middleware/validate.js'), 'utf8')

// Custom validator.
const customValidator = require('../utils/validator')

// Utils.
const fns = require('../utils/functions')

// =============================================================================
// Constants.
// =============================================================================

// Regular expressions.
const ROUTE_REGEX = /app\.(get|post|put|patch|delete)\((.+?)\)/g
const METHOD_REGEX = /app\.(.+?)\(/
const ENDPOINT_REGEX = /\('(.+?)'[\s]*,/
const AUTH_MW_REGEX = /auth\.require\.(.+?),/
const VALIDATION_MW_REGEX = /[\s,(]+validate\.body\.(.+?),/
const COMMENT_REGEX = /^[\s\t]*\/\/(.+?)$/
const FIELDS_TO_OMIT_REGEX = /{[\s]*fieldsToOmit[\s]*:[\s]*(.*?)[\s]*}/
const FIELDS_TO_VALIDATE_REGEX = /{[\s]*fieldsToValidate[\s]*:[\s]*(.*?)[\s]*}/

// =============================================================================
// Class declaration.
// =============================================================================

// DocsGenerator class.
class DocsGenerator {
  constructor () {
    // Doc properties.
    this.docTitle = 'DrumSheetBuilder API Documentation'
    this.docDescription = `Version ${config.project.version}. Last update: ${moment(new Date()).format('YYYY-MM-DD')}. Generated automatically from project code.`
    // Doc file name.
    this.fileName = `greenice-api-${config.project.version}.md`
    // Init class attributes.
    this.routerFiles = {}
    this.routes = []
    this.docs = ''
  }

  init () {
    return new Promise((resolve, reject) => {
      // Find all files inside the router/ folder.
      util.promisify(fs.readdir)(path.resolve(__dirname, '../router'))
        .then(files => {
          // Read every file.
          async.each(files, (file, cb) => {
            // If index.js or admin.js, skip.
            if (path.basename(file) === 'index.js' || path.basename(file) === 'admin.js') return cb()
            // Read the file content.
            util.promisify(fs.readFile)(path.resolve(__dirname, `../router/${file}`), 'utf8')
              .then(content => {
                log.debug(`Found router file ${path.basename(file)}.`)
                this.routerFiles[path.basename(file, path.extname(file))] = content.split(/[\n\r]{1}/)
                cb()
              })
              .catch(cb)
          }, error => {
            if (error) return reject(error)
            resolve()
          })
        })
        .catch(reject)
    })
  }

  // Get validation function from the validation middleware.
  getValidation (middleware) {
    // Get the string parts.
    let parts = middleware.split('.')
    let modelName = parts[0]
    let validationMode = parts[1]
    // Get the fields of the model to validate.
    let modelFields = customValidator[modelName]
    // Get the line of the validation middleware file to parse.
    let re = new RegExp(validationMode + '[ ]*:[ ]*[\\w]*[ ]*\\([ ]*\'' + modelName + '\'(.+)')
    let line = validationMwFile.match(re)[0]
    // Search for keys 'fieldsToValidate' and 'fieldsToOmit'.
    let fieldsToOmit = line.match(FIELDS_TO_OMIT_REGEX)
    let fieldsToValidate = line.match(FIELDS_TO_VALIDATE_REGEX)
    // If fields to omit, return the model list except for these ones.
    if (fieldsToOmit && fieldsToOmit.length) {
      return _.omit(modelFields, JSON.parse(fieldsToOmit[1].replace(/'/g, '"')))
    }
    // If fields to validate, return these.
    if (fieldsToValidate && fieldsToValidate.length) {
      return _.pick(modelFields, JSON.parse(fieldsToValidate[1].replace(/'/g, '"')))
    }
    // If no fields to omit or validate, return all the model fields.
    return modelFields
  }

  // Analyze the routes.
  analyzeRoutes () {
    log.debug(`Analyzing routes.`)
    for (let routerFile in this.routerFiles) {
      if (this.routerFiles.hasOwnProperty(routerFile)) {
        // Loop through every file line.
        for (let i in this.routerFiles[routerFile]) {
          if (this.routerFiles[routerFile].hasOwnProperty(i)) {
            let prevLine = (i !== 0 ? this.routerFiles[routerFile][i - 1] : '')
            let line = this.routerFiles[routerFile][i]
            // If it's a route declaration, append to route list.
            if (ROUTE_REGEX.test(line)) {
              // Find out if any authentication middleware is present.
              let authMiddleware = line.match(AUTH_MW_REGEX)
              // Find out if any validation middleware is present.
              let validationMiddleware = line.match(VALIDATION_MW_REGEX)
              // Get the description.
              let description = prevLine.match(COMMENT_REGEX)
              // Push to routes object.
              this.routes.push({
                file: routerFile,
                method: line.match(METHOD_REGEX)[1].toUpperCase(),
                endpoint: line.match(ENDPOINT_REGEX)[1],
                description: description && description.length ? description[1].trim() : null,
                authRequirements: authMiddleware && authMiddleware.length ? authMiddleware[1] : null,
                validation: validationMiddleware && validationMiddleware.length ? this.getValidation(validationMiddleware[1]) : null
              })
            }
          }
        }
      }
    }
  }

  // Build a Markdown line.
  buildMdLine (content) {
    this.docs += `${content}\n\n`
  }

  // Build the Markdown file.
  buildDocs () {
    // Append header and description to document.
    this.buildMdLine(`# ${this.docTitle}`)
    this.buildMdLine(this.docDescription)
    // Loop through every endpoint.
    for (let i in this.routes) {
      if (this.routes.hasOwnProperty(i)) {
        let route = this.routes[i]
        log.debug(`Building docs for route ${route.method} ${route.endpoint}.`)
        // Build the endpoint title.
        this.buildMdLine(`### ${route.method} ${route.endpoint}`)
        // Build the description.
        if (route.description) this.buildMdLine(route.description)
        // Build the authentication requirements.
        this.buildMdLine(`#### Authentication`)
        if (route.authRequirements) {
          this.buildMdLine(`**Authenticated user**.`)
        } else {
          this.buildMdLine('None.')
        }
        // Build the validation requirements.
        this.buildMdLine(`#### Validation`)
        if (route.validation && _.isObject(route.validation) && !_.isEmpty(route.validation)) {
          this.buildMdLine(`The request body must contain the following parameters:`)
          for (let fieldName in route.validation) {
            if (route.validation.hasOwnProperty(fieldName)) {
              let field = route.validation[fieldName]
              let fieldType = (typeof field.type === 'function' ? field.type.toString().match(/function (.*?)[\s]*\(/)[1] : fns.ucFirst(typeof field.type))
              this.buildMdLine(`- \`${fieldName}\` (${fieldType}): ${field.required ? `**required**` : `optional`}.`)
            }
          }
        } else {
          this.buildMdLine('None.')
        }
      }
    }
  }

  // Execute docs generation.
  exec () {
    return new Promise((resolve, reject) => {
      // Analyze routes.
      this.analyzeRoutes()
      // Build the docs file content.
      this.buildDocs()
      // Write the file.
      util.promisify(fs.writeFile)(path.resolve(__dirname, `../docs/${this.fileName}`), this.docs, 'utf8')
        .then(() => {
          log.info(`File ${chalk.underline(this.fileName)} generated successfully.`)
          resolve()
        })
        .catch(reject)
    })
  }
}

// =============================================================================
// Main.
// =============================================================================

// Declare the script core object.
const core = new ScriptCore()

// Declare the generator object.
const generator = new DocsGenerator()

// Load modules.
core.modules.load()
  .then(() => {
    generator.init()
      .then(() => {
        generator.exec()
          .then(() => {
            core.logElapsedTime()
            process.exit(0)
          })
          .catch(error => {
            ScriptCore.logError(error)
            process.exit(1)
          })
      })
      .catch(error => {
        ScriptCore.logError(error)
        process.exit(1)
      })
  })
  .catch(error => {
    ScriptCore.logError(error)
    process.exit(1)
  })
