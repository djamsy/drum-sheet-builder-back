/**
 * Script to migrate from version 1.5.4 to 1.6.0.
 * The 'projects' collection must contain elements with a new attribute 'time
 * signature', which is calculated from the beats and bars.
 *
 * This script does:
 * - Create new attribute 'timeSignature', which is calculated from 'beats'
 *   and 'bars'.
 * - Remove attributes 'beats' and 'bars'.
 */

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const async = require('async')

// ScriptCore.
const ScriptCore = require('../ScriptCore')

// Project model.
const Project = require('../../model').Project

// =============================================================================
// Class declaration.
// =============================================================================

// Migration class.
class Migration {
  static calculateTimeSignature (beats = 8, bars = 4) {
    return `${beats}/${+bars * 2}`
  }

  static exec () {
    return new Promise((resolve, reject) => {
      // Find all the projects in database.
      Project.find({}, (error, projects) => {
        if (error) return ScriptCore.logError(`Error getting projects`, error)
        log.info(`${projects.length} projects found.`)
        // Do refactor for every project.
        async.each(projects, (p, cb) => {
          // Rebuild the model, if possible.
          if (p.timeSignature) {
            log.info(`Skipping project ${p._id} because it already has a time signature.`)
            return cb()
          }
          p.timeSignature = Migration.calculateTimeSignature(p.beats, p.bars)
          // Save it.
          p.save((error, project) => {
            if (error) ScriptCore.logError(`Error saving project`, error)
            else log.info(`Project ${project._id} updated successfully.`)
            cb()
          })
        }, error => {
          if (error) return reject(error)
          resolve()
        })
      })
    })
  }
}

// =============================================================================
// Main.
// =============================================================================

// Declare the script core object.
const core = new ScriptCore()

// Load modules.
core.modules.load()
  .then(() => {
    Migration.exec()
      .then(() => {
        core.logElapsedTime()
        process.exit(0)
      })
      .catch(error => {
        ScriptCore.logError(`Error updating projects`, error)
        process.exit(1)
      })
  })
  .catch(error => {
    ScriptCore.logError(`Error loading the modules`, error)
    process.exit(1)
  })
