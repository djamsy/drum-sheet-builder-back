/**
 * Class that loads the main files required by scripts.
 */

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const winston = require('winston')

// Modules.
const modules = require('../modules')

// Utils.
const fns = require('../utils/functions')

// =============================================================================
// Initialization. This block must be equal in all migration scripts.
// =============================================================================

// Check for existence of .env file.
try {
  fs.accessSync(path.resolve(__dirname, '../.env'), fs.constants.F_OK)
  require('dotenv').config()
} catch (err) {
  console.log(`${chalk.red('error')} File ${chalk.bold('.env')} not found in ${chalk.underline(path.resolve(__dirname, '..'))}.`)
  process.exit(0)
}

// Get config from read file.
const config = fns.loadConfig(process.env.NODE_ENV)

// Configure winston log.
const log = winston.createLogger({
  level: 'silly', /* always lowest, because we are in a script */
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      )
    })
  ]
})

// Load extra configuration.
fns.loadExtraConfiguration(config)

// Export global variables.
global.config = config
global.log = log

// =============================================================================
// Class declaration.
// =============================================================================

// ScriptCore.
class ScriptCore {
  constructor () {
    this.modules = modules
    this.startTs = Date.now()
  }

  logElapsedTime () {
    // Get the finish date.
    this.endTs = Date.now()
    // Get the elapsed time, in seconds.
    this.elapsedTime = (Math.round(this.endTs - this.startTs) / 1000).toFixed(2)
    // Log the elapsed time.
    log.info(`Execution finished in ${this.elapsedTime} seconds`)
  }

  static logError (error) {
    console.log(fns.stringifyError(error))
  }
}

// Export.
module.exports = ScriptCore
