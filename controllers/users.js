// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const bcrypt = require('bcrypt')

// Entity.
const User = require('../entities/User')

// Utils.
const fns = require('../utils/functions')

// Errors.
const errors = require('../utils/errors')

// =============================================================================
// Main.
// =============================================================================

class UsersController {
  // Get full list of users.
  static all (req, res) {
    log.debug(`User ${req.user.email} getting full user list.`)
    // Get all users.
    User.getAll()
      .then(users => res.api.ok(users))
      .catch(error => {
        log.error(`Database error trying to get user list: ${JSON.stringify(error)}.`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Get details of current user.
  static me (req, res) {
    log.debug(`Getting data from current user: ${req.user.email}.`)
    // Get the desired fields from user and return just those.
    return res.api.ok(User.getSignificantFields(req.user))
  }

  // Save user.
  static async create (req, res, next) {
    log.debug('Creating new user.')
    // Create user object.
    let newUser = {
      email: req.body.email,
      password: await bcrypt.hash(req.body.password, 10) /* hashed */,
      termsAccepted: req.body.termsAccepted
    }
    // Check if user is already registered.
    User.getByEmail(newUser.email)
      .then(result => {
        if (result) {
          log.warn(`Trying to create user with existing e-mail: "${newUser.email}".`)
          return res.api.error(errors.EMAIL_EXISTS.format(newUser.email))
        }
        // Create user.
        User.create(newUser)
          .then(user => {
            log.info(`New user registered: ${newUser.email}.`)
            req.user = user
            return next()
          })
          .catch(error => {
            log.error(`Database error trying to save new user: ${JSON.stringify(error)}.`)
            return res.api.error(fns.getMongooseError(error), error.message)
          })
      })
      .catch(error => {
        log.error(`Database error trying to find user by email "${newUser.email}": ${JSON.stringify(error)}.`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Set a user in recovery state.
  static setRecovery (req, res) {
    log.debug('Setting user in recovery state.')
    // Get user by ID.
    User.getByEmail(req.body.email)
      .then(user => {
        if (!user) {
          log.error(`Trying to set unexisting user with e-mail "${req.body.email}" in recovery state.`)
          return res.api.error(errors.UNEXISTING_EMAIL.format(req.body.email))
        }
        User.setRecovery(user)
          .then(user => {
            log.info(`Recovery token generated successfully for user "${user.email}".`)
            return res.api.ok(User.getSignificantFields(user))
          })
          .catch(error => {
            log.error(`Error while setting user "${req.body.email}" in recovery state: ${JSON.stringify(error)}.`)
            return res.api.error(errors.MAIL_ERROR.format(user.email))
          })
      })
      .catch(error => {
        log.error(`Database error trying to find user by email "${req.body.email}": ${JSON.stringify(error)}.`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Reset a user's password.
  static async resetPassword (req, res) {
    log.debug('Resetting user password.')
    // If no token provided, exit.
    if (!req.body.recoveryToken) {
      log.error(`Trying to reset password of user e-mail "${req.body.email}" without providing a recovery token.`)
      return res.api.error(errors.NO_RECOVERY_TOKEN)
    }
    // Init the new password.
    let newPassword = await bcrypt.hash(req.body.password, 10) /* hashed */
    // Get user by ID.
    User.getByEmail(req.body.email)
      .then(user => {
        if (!user) {
          log.error(`Trying to reset password of unexisting user with e-mail "${req.body.email}".`)
          return res.api.error(errors.UNEXISTING_EMAIL.format(req.body.email))
        }
        // If wrong token, return.
        if (user.recoveryToken !== req.body.recoveryToken) {
          log.error(`Recovery token ${req.body.recoveryToken} is not valid for user "${user.email}"`)
          return res.api.error(errors.WRONG_RECOVERY_TOKEN)
        }
        // Reset the user password.
        User.resetPassword(user, newPassword)
          .then(user => {
            log.info(`User ${user.email} reset his/her password successfully.`)
            return res.api.ok(User.getSignificantFields(user))
          })
          .catch(error => {
            log.error(`Error when trying to reset password of user ${user.email}: ${JSON.stringify(error)}.`)
          })
      })
      .catch(error => {
        log.error(`Database error trying to find user by id ${req.body.email}: ${JSON.stringify(error)}.`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }
}

// Export.
module.exports = UsersController
