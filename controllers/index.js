// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const _ = require('lodash')

// Controllers.
const AuthController = require('./auth')
const UsersController = require('./users')
const ProjectsController = require('./projects')

// Errors.
const errors = require('../utils/errors')

// =============================================================================
// Aux. functions.
// =============================================================================

/**
 * Function to return a JSON object without an error.
 * The child function, that accepts parameter 'data', will eprform the following:
 * - If data is undefined, return an empty JSON response {}.
 * - If data is an object, return it as JSON response.
 * - If data is not an object but is defined, return an object { data: data }.
 */
const apiOk = res => {
  return data => {
    // Init the status code as 204 No Content.
    let statusCode = 204
    // Init the JSON response.
    let json = {}
    // If data, extend the json response with it.
    if (data) {
      // Status code must be 200 OK.
      statusCode = 200
      if (typeof (data) === 'object') {
        // If data is not an object, assign to new attribute 'data'.
        json = data
      } else {
        // If not, extend it also.
        json = { data: data }
      }
    }
    // Return JSON.
    return res.status(statusCode).json(json)
  }
}

/**
 * Function to return a JSON object with an error code, an error message and an
 * errored HTTP status.
 * Parameter 'error' in the child function needs to be an error from errors.js.
 * Second parameter, 'details', is optional, and can contain a description of
 * the error.
 */
const apiError = res => {
  return (error, details) => {
    // Default error status.
    let status = error.status || 500
    // Init the result object.
    let json = { message: error.message }
    // If details, add new property to result object.
    if (details) {
      json.details = details
    }
    // Return JSON.
    return res.status(status).json(json)
  }
}

// =============================================================================
// Main.
// =============================================================================

// Get basic project info.
module.exports.info = (req, res) => {
  res.api.ok(_.pick(config.project, ['name', 'description', 'version']))
}

// Init.
module.exports.init = (req, res, next) => {
  // Init the res.api object
  res.api = {}
  res.api.ok = (data) => apiOk(res)(data)
  res.api.error = (error, details) => apiError(res)(error, details)
  next()
}

// Default 404 fallback.
module.exports.fallback = (req, res) => {
  res.api.error(errors.NOT_FOUND)
}

// =============================================================================
// Export.
// =============================================================================

// Export all controllers.
module.exports = {
  ...module.exports,
  auth: AuthController,
  users: UsersController,
  projects: ProjectsController
}
