// =============================================================================
// Dependencies.
// =============================================================================

// Errors.
const errors = require('../utils/errors')

// Functions.
const fns = require('../utils/functions')

// Sanitizer.
const sanitize = require('../utils/sanitizer')

// Temporary solution.
const Project = require('../entities/Project')

// =============================================================================
// Main.
// =============================================================================

class ProjectsController {
  // Get full list of projects.
  static all (req, res) {
    log.debug(`User ${req.user.email} getting full project list.`)
    // Get all projects.
    Project.getAll()
      .then(projects => res.api.ok(projects))
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find all projects: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Get list of projects from a user.
  static user (req, res) {
    log.debug(`User ${req.user.email} getting his/her project list.`)
    // Find project by user ID.
    Project.getByUserId(req.user._id)
      .then(projects => res.api.ok(projects))
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find projects by userId: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Get details of a project.
  static read (req, res) {
    log.debug(`User ${req.user.email} getting info from project ${req.params.id}.`)
    // Find project by ID.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to get project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to load project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        return res.api.ok(project)
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Save a project.
  static create (req, res) {
    log.debug(`User ${req.user.email} saving new project.`)
    // Get the project.
    let newProject = {
      userId: req.user._id,
      title: sanitize.html.removeTags(req.body.title),
      timeSignature: sanitize.html.removeTags(req.body.timeSignature),
      bpm: sanitize.number.toInt(req.body.bpm),
      depth: sanitize.number.toInt(req.body.depth),
      lines: req.body.lines,
      instrumentOrder: sanitize.html.removeTags(req.body.instrumentOrder),
      instrumentIcons: sanitize.html.removeTags(req.body.instrumentIcons),
      showGrid: sanitize.boolean(req.body.showGrid)
    }
    // Create project.
    Project.create(newProject)
      .then(project => {
        log.info(`User ${req.user.email} created project ${project._id}.`)
        return res.api.ok(project)
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to save project: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Update a project.
  static update (req, res) {
    log.debug(`User ${req.user.email} updating project ${req.params.id}.`)
    // Find existing.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to update project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to update project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Update the project.
        project.title = sanitize.html.removeTags(req.body.title)
        project.timeSignature = sanitize.html.removeTags(req.body.timeSignature)
        project.bpm = sanitize.number.toInt(req.body.bpm)
        project.depth = sanitize.number.toInt(req.body.depth)
        project.lines = req.body.lines
        project.instrumentOrder = sanitize.html.removeTags(req.body.instrumentOrder)
        project.instrumentIcons = sanitize.html.removeTags(req.body.instrumentIcons)
        project.showGrid = sanitize.boolean(req.body.showGrid)
        // Update the project.
        Project.update(project)
          .then(project => {
            log.info(`User ${req.user.email} updated project ${req.params.id}.`)
            return res.api.ok(project)
          })
          .catch(error => {
            log.error(`Database error when user ${req.user.email} tried to save project ${req.params.id}: ${JSON.stringify(error)}`)
            return res.api.error(fns.getMongooseError(error), error.message)
          })
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Delete a project.
  static delete (req, res) {
    log.debug(`User ${req.user.email} deleting project ${req.params.id}.`)
    // Find existing.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to delete project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to delete project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Delete project.
        Project.delete(req.params.id)
          .then(() => {
            log.info(`User ${req.user.email} deleted project ${req.params.id}.`)
            return res.api.ok()
          })
          .catch(error => {
            log.error(`Database error when user ${req.user.email} tried to delete project ${req.params.id}: ${JSON.stringify(error)}`)
            return res.api.error(fns.getMongooseError(error), error.message)
          })
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Generate project sheets.
  static generatePDF (req, res) {
    log.debug(`User ${req.user.email} generating sheets from project ${req.params.id}.`)
    // Get project by ID.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to get project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to generate sheets from project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Render the sheets and send the generated PDF file.
        Project.renderSheets(project, req.user.email, 'dummy')
          .then(fileName => {
            log.info(`User ${req.user.email} generated sheets from project ${req.params.id}: ${fileName}.`)
            return res.api.ok({ url: `/pdf/${req.params.id}` })
          })
          .catch(error => {
            log.error(`Error rendering sheets for project ${req.params.id}: ${error}.`)
            return res.api.error(errors.PDF_RENDER_ERROR, error.message)
          })
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Download sheets from project, that have been generated previously with
  // generatePDF(req, res).
  static downloadPDF (req, res) {
    log.debug(`User ${req.user.email} downloading sheets from project ${req.params.id}.`)
    // Get project by ID.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to get project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to download sheets from project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Get the file location.
        let file = fns.getPdfFileLocation(project._id)
        // Check if the file exists, and then return response consequently.
        fns.fileExists(file)
          .then(file => res.download(file, sanitize.fileName(project.title) + '.pdf'))
          .catch(() => res.api.error(errors.NOT_FOUND))
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Generate project audio.
  static generateWAV (req, res) {
    log.debug(`User ${req.user.email} generating audio in WAV format from project ${req.params.id}.`)
    // Get project by ID.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to get project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to generate audio in WAV format from project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Render the sheets and send the generated PDF file.
        Project.generateAudio(project)
          .then(wavFileName => {
            log.info(`User ${req.user.email} generated audio in WAV format from project ${req.params.id}: ${wavFileName}.`)
            return res.api.ok({ file: `/wav/${req.params.id}` })
          })
          .catch(error => {
            log.error(`Error rendering audio for project ${req.params.id}: ${error}.`)
            return res.api.error(errors.WAV_RENDER_ERROR, error.message)
          })
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Generate project audio in MP3 format.
  static generateMP3 (req, res) {
    log.debug(`User ${req.user.email} generating audio in WAV format from project ${req.params.id}.`)
    // Get project by ID.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to get project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to generate audio in WAV format from project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Render the sheets and send the generated PDF file.
        Project.generateAudio(project)
          .then(wavFileName => {
            log.info(`User ${req.user.email} generated audio in WAV format from project ${req.params.id}: ${wavFileName}.`)
            // Convert to MP3.
            Project.compressAudio(wavFileName, project)
              .then(mp3FileName => {
                log.info(`User ${req.user.email} generated audio in MP3 format from project ${req.params.id}: ${mp3FileName}.`)
                return res.api.ok({ file: `/mp3/${req.params.id}` })
              })
              .catch(error => {
                log.error(`Error converting rendered audio for project ${req.params.id} to MP3: ${error}.`)
                return res.api.error(errors.AUDIO_CONVERSION_ERROR, error.message)
              })
          })
          .catch(error => {
            log.error(`Error rendering audio for project ${req.params.id}: ${error}.`)
            return res.api.error(errors.WAV_RENDER_ERROR, error.message)
          })
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Download audio from project in WAV format, that has been generated
  // previously with generateWAV(req, res).
  static downloadWAV (req, res) {
    log.debug(`User ${req.user.email} downloading audio in WAV format from project ${req.params.id}.`)
    // Get project by ID.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to get project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to download audio in WAV format from project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Get the file location.
        let file = fns.getWavFileLocation(project._id)
        // Check if the file exists, and then return response consequently.
        fns.fileExists(file)
          .then(file => res.download(file, sanitize.fileName(project.title) + '.wav'))
          .catch(() => res.api.error(errors.NOT_FOUND))
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Download audio from project in MP3 format, that has been generated
  // previously with generateWAV(req, res).
  static downloadMP3 (req, res) {
    log.debug(`User ${req.user.email} downloading audio in MP3 format from project ${req.params.id}.`)
    // Get project by ID.
    Project.getById(req.params.id)
      .then(project => {
        if (!project) {
          log.info(`User ${req.user.email} tried to get project ${req.params.id}, but project was not found.`)
          return res.api.error(errors.NOT_FOUND)
        }
        if (!fns.equalObjectIds(project.userId, req.user._id)) {
          log.warn(`User ${req.user.email} tried to download audio in MP3 format from project ${req.params.id}, but is not the owner.`)
          return res.api.error(errors.NOT_AUTHORIZED)
        }
        // Get the file location.
        let file = fns.getMp3FileLocation(project._id)
        // Check if the file exists, and then return response consequently.
        fns.fileExists(file)
          .then(file => res.download(file, sanitize.fileName(project.title) + '.mp3'))
          .catch(() => res.api.error(errors.NOT_FOUND))
      })
      .catch(error => {
        log.error(`Database error when user ${req.user.email} tried to find project by id ${req.params.id}: ${JSON.stringify(error)}`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }
}

// Export.
module.exports = ProjectsController
