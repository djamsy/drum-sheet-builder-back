// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const bcrypt = require('bcrypt')

// Entity.
const User = require('../entities/User')

// Utils.
const fns = require('../utils/functions')

// Errors.
const errors = require('../utils/errors')

// =============================================================================
// Main.
// =============================================================================

class AuthController {
  // Verify user before generating JWT.
  static verify (req, res, next) {
    log.debug(`User ${req.body.email} logging in.`)
    // Create user object.
    let authUser = {
      email: req.body.email,
      password: req.body.password
    }
    // Find in database.
    User.getByEmail(authUser.email)
      .then(result => {
        // Notify only about the wrong combination of user/password.
        // Do NOT notify which of the fields is wrong, due to security issues.
        if (!result) {
          log.warn(`User ${authUser.email} tried to log in but is not registered.`)
          return res.api.error(errors.WRONG_AUTH)
        }
        // Compare passwords.
        bcrypt.compare(authUser.password, result.password)
          .then(equalPasswords => {
            // If passwords don't match.
            if (!equalPasswords) {
              log.warn(`User ${authUser.email} tried to log in with wrong password.`)
              return res.api.error(errors.WRONG_AUTH)
            }
            // Return.
            req.user = result
            return next()
          })
          .catch(error => {
            log.error(`Error comparing bcrypt passwords when user ${authUser.email} logging in: ${JSON.stringify(error)}`)
            res.api.error(errors.UNKNONW_ERROR)
          })
      })
      .catch(error => {
        log.error(`Database error trying to get user by email ${authUser.email}: ${JSON.stringify(error)}.`)
        return res.api.error(fns.getMongooseError(error), error.message)
      })
  }

  // Generate JWT.
  static login (req, res) {
    // Generate the user token.
    User.encode(req.user, req.headers.origin)
      .then(encodedUser => {
        log.info(`User ${encodedUser.email} logged in.`)
        // Return JSON response.
        return res.api.ok(encodedUser)
      })
  }
}

// Export.
module.exports = AuthController
